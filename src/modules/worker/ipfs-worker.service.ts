import {NftOnchain} from '../../database/entities';
import {getLogger} from '../../shared/logger';
import {DataSource, getConnection} from 'typeorm';
import {nowInMillis} from 'src/shared/Utils';
import pLimit from 'p-limit';
import {S3Handler} from 'src/shared/S3Handler';

const fs = require('fs')
const Web3 = require("web3");
const logger = getLogger('IPFSWorkerService');
const axios = require("axios");
const sharp = require('sharp');
const BATCH_LIMIT = 5;
const limit = pLimit(BATCH_LIMIT);
const RESIZE_MAX = 600;  // 600px
const MIN_UPDATE_TIME = 1000 * 60 * 60 * 2400;  // 100 days
const NodeCache = require("node-cache");
const nodeCache = new NodeCache({stdTTL: 86400, checkperiod: 86400});

export class IPFSWorkerService {

    _nftAbi = fs.readFileSync('./smart-contract/ERC20.json', 'utf8');

    constructor(
        private readonly s3handler: S3Handler,
        private readonly dataSource: DataSource
    ) {
        this._setup();
    }

    async delay(t) {
        return new Promise(resolve => setTimeout(resolve, t));
    }

    async _setup() {
        do {
            try {
                let isWaiting = await this.crawlData();
                if (isWaiting) {
                    await this.delay(5000); // 5 seconds, to avoid too many requests
                }

            } catch (e) {
                if (e.message.indexOf('ER_LOCK_WAIT_TIMEOUT') > -1 || e.message.indexOf('ER_LOCK_DEADLOCK') > -1) {
                    logger.info(`IPFSWorkerService::doCrawlJob Other server is doing the job, wait for a while`);
                } else {
                    logger.error(`IPFSWorkerService::doCrawlJob ${e.message}`);
                }
            }
        } while (true);
    }

    async crawlData() {

        return await this.dataSource.transaction(async (manager) => {
            let nftOnchains = await manager
                .getRepository(NftOnchain)
                .createQueryBuilder('nft_onchain')
                .useTransaction(true)
                .setLock("pessimistic_write")
                .where("nft_onchain.origin_image IS NOT NULL")
                .andWhere("nft_onchain.ipfs_updated_at < :maxTime", {maxTime: nowInMillis() - MIN_UPDATE_TIME})
                .andWhere("nft_onchain.is_ipfs_updated = 0")
                .orderBy('nft_onchain.ipfs_updated_at', 'ASC')
                .limit(BATCH_LIMIT)
                .getMany();

            await Promise.all(
                nftOnchains.map(async (nftOnchain) => {
                    return limit(async () => {
                        try {
                            logger.info(`IPFSWorkerService::crawlData start get MetaData nftOnchain id: ${nftOnchain.id}`);
                            let resizedUrl = await this.functionCache(nftOnchain.originImage, this.convertUrl(nftOnchain.originImage))

                            await manager.getRepository(NftOnchain).update({id: nftOnchain.id}, {
                                image: resizedUrl,
                                ipfsUpdatedAt: nowInMillis(),
                                isIpfsUpdated: true
                            });
                            return true;
                        } catch (ex) {
                            // logger.error(ex);
                            try {
                                await manager.getRepository(NftOnchain).update({id: nftOnchain.id},
                                    {
                                        ipfsUpdatedAt: nowInMillis(),
                                        isIpfsUpdated: false
                                    });
                            } catch (ex2) {
                            }
                        }
                    })
                }));

            return true;
        });
    }

    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
    }

    async convertUrl(url: string) {
        await this.delay(this.getRandomInt(0, 5000)); // avoid to many requests.
        let originData = null;
        try {
            originData = await axios.get(url, {responseType: "arraybuffer"});
        } catch (e) {
            if (e.response.status = 429) {
                return this.convertUrl(url);
            }
        }
        let image = sharp(originData.data)
        const metadata = await image.metadata()
        if (metadata.width > RESIZE_MAX || metadata.height > RESIZE_MAX) {
            image = await image.resize(RESIZE_MAX)
        }
        const resizedData = await image.png().toBuffer();

        let urlObject = new URL(url)
        let pathname = urlObject.pathname;
        if (pathname[0] === '/') {
            pathname = pathname.substring(1);
        }
        const imageUpload = await this.s3handler.upload(`ipfs`, {originalname: pathname, buffer: resizedData});

        return imageUpload.Location;
    }

    async functionCache(key, func) {
        let value = nodeCache.get(key);
        if (value == undefined) {
            // handle miss!
            value = await func;
            nodeCache.set(key, value);
            return value;
        }
        return value;
    }
}
