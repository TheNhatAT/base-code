import {OnchainStatus} from '../../shared/enums';
import * as _ from 'lodash';
import {Collection, CurrencyConfig, LatestBlock, NftLog, NftOwner} from '../../database/entities';
import {getLogger} from '../../shared/logger';
import {DataSource, getConnection, LessThanOrEqual} from 'typeorm';
import * as ethereumjs from '@ethereumjs/tx';
import Common from "@ethereumjs/common";
import {NotificationService} from '../notification/notification.service';
import {NftOnchain} from 'src/database/entities/NftOnchain.entity';
import {SocketService} from './socket.service';
import {getBlockNumber, nowInMillis} from 'src/shared/Utils';
import pLimit from 'p-limit';
import {CollectionSyncStatus, NftStatus} from 'src/shared/enums';

const NodeCache = require("node-cache");
const BATCH_LIMIT = 20;
const nodeCache = new NodeCache({stdTTL: BATCH_LIMIT, checkperiod: BATCH_LIMIT});

const EthereumTx = ethereumjs.Transaction;
const Web3 = require("web3");
const fs = require('fs')
const axios = require("axios");
const logger = getLogger('CrawlWorkerService');
const RETRY_INTERVAL = 1 * 60 * 1000; // 1 minutes
const limit = pLimit(BATCH_LIMIT);

export class CrawlWorkerService {

    _web3 = new Web3(this.currency.rpcEndpoint);

    _common = Common.custom({chainId: Number(this.currency.chainId)});

    _nft20Abi = fs.readFileSync('./smart-contract/ERC20.json', 'utf8');
    _nft1155Abi = fs.readFileSync('./smart-contract/ERC1155.json', 'utf8');
    _nftContract = new this._web3.eth.Contract(JSON.parse(this.collection.type == "ERC721" ? this._nft20Abi : this._nft1155Abi), this.collection.address);

    _addminAddress = null;
    public isStopped = false;
    public isDelisted = false;

    constructor(
        private currency: CurrencyConfig,
        private readonly collection: Collection,
        private readonly notificationService: NotificationService,
        private readonly socketService: SocketService,
        private readonly dataSource: DataSource
    ) {
        // if (this.currency.network === 'polygon') {
        this._setup();
        // }
    }

    async _setup() {
        this.doCrawlJob();
    }

    async delay(t) {
        return new Promise(resolve => setTimeout(resolve, t));
    }

    async doCrawlJob() {
        do {
            try {
                // test send socket
                //this.socketService.sendToSocket({ xxx: 'xxx', yyy: 'yyy' }, "test");

                let isWaiting = await this.crawlData();
                if (isWaiting) {
                    await this.delay(this.currency.averageBlockTime);
                } else {
                    await this.delay(300); // 0.5 seconds, to avoid too many requests
                    if (isWaiting === 0) {
                        // exit loop
                        this.isStopped = true;
                        break;
                    }
                }
            } catch (e) {
                console.log(e)
                if (e.message.indexOf('ER_LOCK_WAIT_TIMEOUT') > -1 || e.message.indexOf('ER_LOCK_DEADLOCK') > -1) {
                    logger.info(`${this.currency.network} CrawlWorkerService::doCrawlJob Other server is doing the job, wait for a while`);
                } else {
                    logger.error(`${this.currency.network} CrawlWorkerService::doCrawlJob ${e.message}`);
                    this.notificationService.notificationException(`${this.currency.network} CrawlWorkerService::doCrawlJob ${e.message}`);
                }
            }
        } while (true);
        this.isStopped = true;
    }

    /**
     * Step 1: Get the data from the blockchain
     * @returns {Promise<void>}
     */
    async crawlData() {
        return await this.dataSource.transaction(async (manager) => {
            let latestBlockInDb = await manager
                .getRepository(LatestBlock)
                .createQueryBuilder('latest_block')
                .useTransaction(true)
                .setLock("pessimistic_write")
                .where({currency: "crawl_" + this.currency.network + "_" + this.collection.address})
                .getOne();

            let latestTempBlockInDb = await manager.getRepository(LatestBlock).findOne({
                where: {
                    currency: "crawl_temp_" + this.currency.network + "_" + this.collection.address
                }
            });
            const latestBlock = await getBlockNumber(this.currency.chainId, this._web3);

            const tempSyncStatusCollection = this.collection.syncStatus;
            // handle check sync status of collection
            if (latestTempBlockInDb && latestBlock - latestTempBlockInDb.blockNumber < this.currency.requiredConfirmations) {
                this.collection.syncStatus = CollectionSyncStatus.SYNCED;
            } else {
                this.collection.syncStatus = CollectionSyncStatus.SYNCING;
            }
            if (this.collection.syncStatus && (tempSyncStatusCollection != this.collection.syncStatus)) {
                await manager.getRepository(Collection).save(this.collection);
            }

            if (this.isDelisted || !latestBlockInDb) {
                let collectionStatus = await manager.getRepository(Collection).count({
                    where: {
                        chainId: this.currency.chainId,
                        address: this.collection.address,
                        status: "listed"
                    }
                });
                logger.info(`${this.currency.network} CrawlWorkerService::crawlData collection ${this.collection.name} collectionStatus ${collectionStatus}`);
                if (!collectionStatus) {
                    logger.info(`${this.currency.network} CrawlWorkerService::crawlData collection ${this.collection.name} is delisted`);
                    await manager.getRepository(Collection).delete({
                        chainId: this.currency.chainId,
                        address: this.collection.address
                    });
                    await manager.getRepository(NftOnchain).delete({
                        chainId: this.currency.chainId,
                        contractAddress: this.collection.address
                    });
                    await manager.getRepository(NftLog).delete({
                        chainId: this.currency.chainId,
                        contractAddress: this.collection.address
                    });
                    await manager.getRepository(NftOwner).delete({
                        chainId: this.currency.chainId,
                        contractAddress: this.collection.address
                    });
                    await manager.getRepository(LatestBlock).delete({currency: "crawl_" + this.currency.network + "_" + this.collection.address});
                    await manager.getRepository(LatestBlock).delete({currency: "crawl_temp_" + this.currency.network + "_" + this.collection.address});
                    return 0;
                }
            }

            if (!latestBlockInDb) {
                latestBlockInDb = new LatestBlock();
                latestBlockInDb.currency = "crawl_" + this.currency.network + "_" + this.collection.address;
                let getFirstBlockUrl = this.currency.scanApi
                    + `/api?module=account&action=txlist&address=${this.collection.address}&startblock=0&endblock=99999999&page=1&offset=1&sort=asc`
                let firstBlock = await axios.get(getFirstBlockUrl);

                if (firstBlock) {
                    await this.delay(5000);
                }

                latestBlockInDb.blockNumber = firstBlock.data.result[0]?.blockNumber;

                if (latestBlockInDb.blockNumber) {
                    await manager.getRepository(LatestBlock).save(latestBlockInDb);
                }
                await manager.delete(NftLog, {
                    chainId: this.currency.chainId,
                    contractAddress: this.collection.address
                });
                await manager.delete(NftOwner, {
                    chainId: this.currency.chainId,
                    contractAddress: this.collection.address
                });
                await manager.delete(NftOnchain, {
                    chainId: this.currency.chainId,
                    contractAddress: this.collection.address
                });
                return false;
            }
            if (!latestTempBlockInDb) {
                latestTempBlockInDb = new LatestBlock();
                latestTempBlockInDb.currency = "crawl_temp" + this.currency.network + "_" + this.collection.address;
                latestTempBlockInDb.blockNumber = latestBlockInDb.blockNumber;
            }

            let fromBlock = latestBlockInDb.blockNumber + 1;
            let toBlock = latestBlock - this.currency.requiredConfirmations;

            // max crawl many blocks per time
            if (toBlock > fromBlock + 999) {
                toBlock = fromBlock + 999;
            }

            let tempFromBlock = Math.max(toBlock + 1, latestTempBlockInDb.blockNumber + 1);
            let tempToBlock = latestBlock - this.currency.tempRequiredConfirmations;
            // max crawl many blocks per time
            if (tempToBlock > tempFromBlock + 999) {
                tempToBlock = tempFromBlock + 999;
            }


            if (fromBlock <= toBlock) {
                logger.info(`${this.currency.network} CrawlWorkerService::crawlData ${this.collection.name} Crawling from block ${fromBlock} => ${toBlock} (lastest block: ${latestBlock})`);
                if (this.collection.type == "ERC721") {
                    await this.crawlBlock721(manager, fromBlock, toBlock, latestBlock, false);
                    await this.crawlBlock721(manager, tempFromBlock, tempToBlock, latestBlock, true);
                } else {
                    await this.crawlBlock1155(manager, fromBlock, toBlock, latestBlock, false);
                    await this.crawlBlock1155(manager, tempFromBlock, tempToBlock, latestBlock, true);
                }
            }

            return toBlock - fromBlock > 1;

        });
    }

    async crawlBlock721(manager, _fromBlock: number, _toBlock: number, _latestBlock: number, _isTemp: boolean = false) {
        if (!_isTemp || (_latestBlock - _toBlock <= this.currency.requiredConfirmations)) {
            let transferEvents = [];
            if (this._nftContract._address) {
                transferEvents = await this._nftContract.getPastEvents('Transfer', {
                    fromBlock: _fromBlock,
                    toBlock: _toBlock
                });
            }

            logger.info(`${this.currency.network} CrawlWorkerService::crawlBlock721 ${this.collection.name} Crawling ${transferEvents.length} events from block ${_fromBlock} => ${_toBlock} (lastest block: ${_latestBlock})`);
            // loop events
            await Promise.all(
                transferEvents.map(async (transferEvent) => {
                    return limit(async () => {
                        //create new Transaction
                        let tokenId = transferEvent.returnValues.tokenId;
                        let tokenUri = null;
                        try {
                            tokenUri = await this._nftContract.methods.tokenURI(tokenId).call();
                        } catch (ex) {
                            if (ex.message.indexOf('Returned error: execution reverted') > -1) {
                                try {
                                    tokenUri = await this._nftContract.methods.uri(tokenId).call();
                                } catch (ex) {
                                    if (ex.message.indexOf('Returned error: execution reverted') > -1) {
                                        tokenUri = "";
                                    } else {
                                        throw ex;
                                    }
                                }
                            } else {
                                throw ex;
                            }
                        }
                        const blockData: any = await this.web3Cache("getBlock_" + transferEvent.blockNumber, this._web3.eth.getBlock(transferEvent.blockNumber));

                        let status = _isTemp ? OnchainStatus.CONFIRMING : OnchainStatus.CONFIRMED;
                        let nftOnchain = new NftOnchain();
                        nftOnchain.id = null; // FOR NO_AUTO_VALUE_ON_ZERO (reference from: https://github.com/typeorm/typeorm/issues/7643#issuecomment-1086861654)
                        nftOnchain.chainId = this.currency.chainId;
                        nftOnchain.contractAddress = this._nftContract._address;
                        nftOnchain.tokenId = tokenId;
                        nftOnchain.tokenUri = tokenUri;
                        nftOnchain.blockTimestamp = blockData.timestamp;
                        nftOnchain.metadataUpdatedAt = nowInMillis() - 1000000000000;
                        nftOnchain.ipfsUpdatedAt = 0;
                        nftOnchain.status = status;
                        nftOnchain.displayStatus = NftStatus.LISTED;

                        if (transferEvent.returnValues.from == "0x0000000000000000000000000000000000000000") {
                            const txReceipt: any = await this.web3Cache("getTransactionReceipt_" + transferEvent.transactionHash, this._web3.eth.getTransactionFromBlock(transferEvent.blockNumber, transferEvent.transactionIndex));
                            nftOnchain.creator = txReceipt.from;
                            await manager.createQueryBuilder().insert()
                                .into(NftOnchain)
                                .values(nftOnchain)
                                .orUpdate(['token_uri', 'block_timestamp', 'status', 'creator'], ['chainId', 'contractAddress', 'tokenId'])
                                .execute();
                        } else {
                            await manager.createQueryBuilder().insert()
                                .into(NftOnchain)
                                .values(nftOnchain)
                                .orUpdate(['token_uri', 'block_timestamp', 'status'], ['chainId', 'contractAddress', 'tokenId'])
                                .execute();
                        }

                        let owner = await this._nftContract.methods.ownerOf(tokenId).call();
                        if (transferEvent.returnValues.to !== "0x0000000000000000000000000000000000000000") {
                            let nftOwner = new NftOwner();
                            nftOwner.chainId = this.currency.chainId;
                            nftOwner.contractAddress = this._nftContract._address;
                            nftOwner.tokenId = tokenId;
                            nftOwner.owner = transferEvent.returnValues.to;
                            nftOwner.amount = transferEvent.returnValues.to == owner ? 1 : 0;
                            nftOwner.blockTimestamp = blockData.timestamp;
                            nftOwner.status = status;
                            await manager.createQueryBuilder().insert()
                                .into(NftOwner)
                                .values(nftOwner)
                                .orUpdate(['amount', 'status'], ['chainId', 'contractAddress', 'tokenId', 'nftOwner'])
                                .execute();
                        }

                        if (transferEvent.returnValues.from !== "0x0000000000000000000000000000000000000000") {
                            let nftOldOwner = new NftOwner();
                            nftOldOwner.chainId = this.currency.chainId;
                            nftOldOwner.contractAddress = this._nftContract._address;
                            nftOldOwner.tokenId = tokenId;
                            nftOldOwner.owner = transferEvent.returnValues.from;
                            nftOldOwner.amount = transferEvent.returnValues.from == owner ? 1 : 0;
                            nftOldOwner.blockTimestamp = blockData.timestamp;
                            nftOldOwner.status = status;
                            await manager.createQueryBuilder().insert()
                                .into(NftOwner)
                                .values(nftOldOwner)
                                .orUpdate(['amount', 'status'], ['chainId', 'contractAddress', 'tokenId', 'nftOwner'])
                                .execute();
                        }

                        let nftLog = new NftLog();
                        nftLog.chainId = this.currency.chainId;
                        nftLog.tokenId = tokenId;
                        nftLog.contractAddress = this._nftContract._address;
                        nftLog.from = transferEvent.returnValues.from;
                        nftLog.to = transferEvent.returnValues.to;
                        nftLog.status = status;
                        nftLog.action = transferEvent.returnValues.from === "0x0000000000000000000000000000000000000000" ? "mint" :
                            (transferEvent.returnValues.to === "0x0000000000000000000000000000000000000000" ? "burn" : "transfer");
                        nftLog.amount = 1;
                        nftLog.blockNumber = transferEvent.blockNumber;
                        nftLog.txid = transferEvent.transactionHash;
                        nftLog.blockHash = transferEvent.blockHash;
                        nftLog.blockTimestamp = blockData.timestamp;

                        logger.info(`${this.currency.network} CrawlWorkerService::crawlBlock${_isTemp ? "Temp" : ""} Update token ${JSON.stringify(nftLog)}`);
                        await manager.save(nftLog);
                    })
                }));
        }

        if (!_isTemp) {
            await manager.delete(NftOwner, {
                chainId: this.currency.chainId,
                contractAddress: this.collection.address,
                amount: 0
            });
            await manager.delete(NftLog, {
                chainId: this.currency.chainId,
                contractAddress: this.collection.address,
                status: "confirming",
                blockNumber: LessThanOrEqual(_toBlock)
            });
        }

        // update latest block in transaction
        const latestBlockKey = _isTemp ? ("crawl_temp_" + this.currency.network + "_" + this.collection.address) : ("crawl_" + this.currency.network + "_" + this.collection.address)
        let latestBlock = await manager.findOne(LatestBlock, {currency: latestBlockKey});
        if (!latestBlock) {
            latestBlock = new LatestBlock();
            latestBlock.currency = latestBlockKey;
        }
        latestBlock.blockNumber = _toBlock;
        await manager.save(latestBlock);
    }

    async crawlBlock1155(manager, _fromBlock: number, _toBlock: number, _latestBlock: number, _isTemp: boolean = false) {
        if (!_isTemp || (_latestBlock - _toBlock <= this.currency.requiredConfirmations)) {
            let transferSingleEvents = [];
            if (this._nftContract._address) {
                transferSingleEvents = await this._nftContract.getPastEvents('TransferSingle', {
                    fromBlock: _fromBlock,
                    toBlock: _toBlock
                });
            }
            let transferBatchEvents = [];
            if (this._nftContract._address) {
                transferBatchEvents = await this._nftContract.getPastEvents('TransferBatch', {
                    fromBlock: _fromBlock,
                    toBlock: _toBlock
                });
            }

            for (const transferSingleEvent of transferSingleEvents) {
                transferSingleEvent.returnValues.ids = [transferSingleEvent.returnValues.id];
                transferSingleEvent.returnValues.values = [transferSingleEvent.returnValues.value];
            }
            transferBatchEvents = transferBatchEvents.concat(transferSingleEvents);

            // loop events
            await Promise.all(
                transferBatchEvents.map(async (transferEvent) => {
                    return limit(async () => {
                        for (let i = 0; i < transferEvent.returnValues.ids.length; i++) {
                            //create new Transaction
                            let tokenId = transferEvent.returnValues.ids[i];
                            let tokenUri = null;
                            try {
                                tokenUri = await this._nftContract.methods.tokenURI(tokenId).call();
                            } catch (ex) {
                                if (ex.message.indexOf('Returned error: execution reverted') > -1) {
                                    try {
                                        tokenUri = await this._nftContract.methods.uri(tokenId).call();
                                    } catch (ex) {
                                        if (ex.message.indexOf('Returned error: execution reverted') > -1) {
                                            tokenUri = "";
                                        } else {
                                            throw ex;
                                        }
                                    }
                                } else {
                                    throw ex;
                                }
                            }

                            const blockData: any = await this.web3Cache("getBlock_" + transferEvent.blockNumber, this._web3.eth.getBlock(transferEvent.blockNumber));

                            let status = _isTemp ? OnchainStatus.CONFIRMING : OnchainStatus.CONFIRMED;
                            let nftOnchain = new NftOnchain();
                            nftOnchain.id = null; // FOR NO_AUTO_VALUE_ON_ZERO (reference from: https://github.com/typeorm/typeorm/issues/7643#issuecomment-1086861654)
                            nftOnchain.chainId = this.currency.chainId;
                            nftOnchain.contractAddress = this._nftContract._address;
                            nftOnchain.tokenId = tokenId;
                            nftOnchain.tokenUri = tokenUri;
                            nftOnchain.blockTimestamp = blockData.timestamp;
                            nftOnchain.metadataUpdatedAt = nowInMillis() - 1000000000000;
                            nftOnchain.ipfsUpdatedAt = 0;
                            nftOnchain.status = status;
                            nftOnchain.displayStatus = NftStatus.LISTED;

                            if (transferEvent.returnValues.from == "0x0000000000000000000000000000000000000000") {
                                const txReceipt: any = await this.web3Cache("getTransactionReceipt_" + transferEvent.transactionHash, this._web3.eth.getTransactionFromBlock(transferEvent.blockNumber, transferEvent.transactionIndex));
                                nftOnchain.creator = txReceipt.from;
                                await manager.createQueryBuilder().insert()
                                    .into(NftOnchain)
                                    .values(nftOnchain)
                                    .orUpdate(['token_uri', 'block_timestamp', 'status', 'creator'], ['chainId', 'contractAddress', 'tokenId'])
                                    .execute();
                            } else {
                                await manager.createQueryBuilder().insert()
                                    .into(NftOnchain)
                                    .values(nftOnchain)
                                    .orUpdate(['token_uri', 'block_timestamp', 'status'], ['chainId', 'contractAddress', 'tokenId'])
                                    .execute();
                            }

                            if (transferEvent.returnValues.to !== "0x0000000000000000000000000000000000000000") {
                                let nftOwner = new NftOwner();
                                nftOwner.chainId = this.currency.chainId;
                                nftOwner.contractAddress = this._nftContract._address;
                                nftOwner.tokenId = tokenId;
                                nftOwner.owner = transferEvent.returnValues.to;
                                nftOwner.amount = await this._nftContract.methods.balanceOf(transferEvent.returnValues.to, tokenId).call();
                                nftOwner.blockTimestamp = blockData.timestamp;
                                nftOwner.status = status;
                                await manager.createQueryBuilder().insert()
                                    .into(NftOwner)
                                    .values(nftOwner)
                                    .orUpdate(['amount', 'status'], ['chainId', 'contractAddress', 'tokenId', 'nftOwner'])
                                    .execute();
                            }

                            if (transferEvent.returnValues.from !== "0x0000000000000000000000000000000000000000") {
                                let nftOldOwner = new NftOwner();
                                nftOldOwner.chainId = this.currency.chainId;
                                nftOldOwner.contractAddress = this._nftContract._address;
                                nftOldOwner.tokenId = tokenId;
                                nftOldOwner.owner = transferEvent.returnValues.from;
                                nftOldOwner.amount = await this._nftContract.methods.balanceOf(transferEvent.returnValues.from, tokenId).call();
                                nftOldOwner.blockTimestamp = blockData.timestamp;
                                nftOldOwner.status = status;
                                await manager.createQueryBuilder().insert()
                                    .into(NftOwner)
                                    .values(nftOldOwner)
                                    .orUpdate(['amount', 'status'], ['chainId', 'contractAddress', 'tokenId', 'nftOwner'])
                                    .execute();
                            }

                            let nftLog = new NftLog();
                            nftLog.chainId = this.currency.chainId;
                            nftLog.tokenId = tokenId;
                            nftLog.contractAddress = this._nftContract._address;
                            nftLog.from = transferEvent.returnValues.from;
                            nftLog.to = transferEvent.returnValues.to;
                            nftLog.status = status;
                            nftLog.action = transferEvent.returnValues.from === "0x0000000000000000000000000000000000000000" ? "mint" :
                                (transferEvent.returnValues.to === "0x0000000000000000000000000000000000000000" ? "burn" : "transfer");
                            nftLog.amount = transferEvent.returnValues.values[i];
                            nftLog.blockNumber = transferEvent.blockNumber;
                            nftLog.txid = transferEvent.transactionHash;
                            nftLog.blockHash = transferEvent.blockHash;
                            nftLog.blockTimestamp = blockData.timestamp;

                            await manager.save(nftLog);
                        }
                    })
                }));

        }

        if (!_isTemp) {
            await manager.delete(NftOwner, {
                chainId: this.currency.chainId,
                contractAddress: this.collection.address,
                amount: 0
            });
            await manager.delete(NftLog, {
                chainId: this.currency.chainId,
                contractAddress: this.collection.address,
                status: "confirming",
                blockNumber: LessThanOrEqual(_toBlock)
            });
        }

        // update latest block in transaction
        const latestBlockKey = _isTemp ? ("crawl_temp_" + this.currency.network + "_" + this.collection.address) : ("crawl_" + this.currency.network + "_" + this.collection.address)
        let latestBlock = await manager.findOne(LatestBlock, {currency: latestBlockKey});


        if (!latestBlock) {
            latestBlock = new LatestBlock();
            latestBlock.currency = latestBlockKey;
        }
        latestBlock.blockNumber = _toBlock;
        await manager.save(latestBlock);
    }

    async web3Cache(key, func) {
        let value = nodeCache.get(key);
        if (value == undefined) {
            // handle miss!
            value = await func;
            nodeCache.set(key, value);
            return value;
        }
        return value;
    }
}
