import {CurrencyConfig, NftOnchain} from '../../database/entities';
import {getLogger} from '../../shared/logger';
import {DataSource, getConnection} from 'typeorm';
import {getContentTypeByURL, nowInMillis} from 'src/shared/Utils';
import {setup} from 'axios-cache-adapter'
import pLimit from 'p-limit';

const fs = require('fs')
const Web3 = require("web3");
const logger = getLogger('MetaDataWorkerService');
const axios = setup({
    cache: {
        maxAge: 5 * 60 * 1000 // 5 minutes
    }
});
const BATCH_LIMIT = 20;
const limit = pLimit(BATCH_LIMIT);

export class MetaDataWorkerService {

    _nftAbi = fs.readFileSync('./smart-contract/ERC20.json', 'utf8');

    constructor(
        private readonly dataSource: DataSource
    ) {
        this._setup();
    }

    async delay(t) {
        return new Promise(resolve => setTimeout(resolve, t));
    }

    async _setup() {
        do {
            try {
                let isWaiting = await this.crawlData();
                if (isWaiting) {
                    await this.delay(5000); // 5 seconds, to avoid too many requests
                }

            } catch (e) {
                if (e.message.indexOf('ER_LOCK_WAIT_TIMEOUT') > -1 || e.message.indexOf('ER_LOCK_DEADLOCK') > -1) {
                    logger.info(`MetaDataWorkerService::doCrawlJob Other server is doing the job, wait for a while`);
                } else {
                    logger.error(`MetaDataWorkerService::doCrawlJob ${e.message}`);
                }
            }
        } while (true);
    }

    async crawlData() {

        return await this.dataSource.transaction(async (manager) => {
            let nftOnchains = await manager
                .getRepository(NftOnchain)
                .createQueryBuilder('nft_onchain')
                .useTransaction(true)
                .setLock("pessimistic_write")
                .where("nft_onchain.is_metadata_updated = 0")
                .orderBy('nft_onchain.metadata_updated_at', 'ASC')
                .limit(BATCH_LIMIT)
                .getMany();

            await Promise.all(
                nftOnchains.map(async (nftOnchain) => {
                    return limit(async () => {
                        try {
                            logger.info(`MetaDataWorkerService::crawlData start get MetaData nftOnchain id: ${nftOnchain.id}`);

                            let currency = await manager.getRepository(CurrencyConfig).findOne({
                                where: {
                                    chainId: nftOnchain.chainId
                                }
                            });
                            let web3 = new Web3(currency.rpcEndpoint);

                            let nftContract = new web3.eth.Contract(JSON.parse(this._nftAbi), nftOnchain.contractAddress);
                            let tokenUri = null;
                            try {
                                tokenUri = await nftContract.methods.tokenURI(nftOnchain.tokenId).call();
                            } catch (ex) {
                                if (ex.message.indexOf('Returned error: execution reverted') > -1) {
                                    try {
                                        tokenUri = await nftContract.methods.uri(nftOnchain.tokenId).call();
                                    } catch (ex) {
                                        if (ex.message.indexOf('Returned error: execution reverted') > -1) {
                                            tokenUri = "";
                                        } else {
                                            throw ex;
                                        }
                                    }
                                } else {
                                    throw ex;
                                }
                            }

                            let metadataReponse: any = "";
                            if (tokenUri && tokenUri.length > 0) {
                                logger.info(`MetaDataWorkerService::crawlData axios get metadata: ${tokenUri}`);
                                let metadata = await axios.get(tokenUri);
                                if (metadata && metadata.data) {
                                    try {
                                        metadataReponse = JSON.parse(Buffer.from(metadata.data, 'base64').toString());
                                        logger.info(`MetaDataWorkerService::crawlData axios get metadata: ${tokenUri} success`);
                                    } catch (error) {
                                        logger.info(`MetaDataWorkerService::crawlData axios get metadata: ${tokenUri} fail ${error.message}`);
                                        try {
                                            metadataReponse = metadata.data;
                                        } catch (error) {
                                            logger.info(`MetaDataWorkerService::crawlData axios get metadata: ${tokenUri} fail ${error.message}`);
                                            // return { balance, collectionName, dataFromUri: {}, maxSupply, uri, owner };
                                        }
                                    }
                                }
                            }

                            let contentType;
                            if (metadataReponse && metadataReponse.image) {
                                contentType = await getContentTypeByURL(metadataReponse.image);
                            }

                            await manager.getRepository(NftOnchain).update({id: nftOnchain.id}, {
                                tokenUri: tokenUri,
                                metadata: JSON.stringify(metadataReponse),
                                name: metadataReponse?.name,
                                image: metadataReponse?.image == nftOnchain.originImage ? nftOnchain.image : metadataReponse?.image,
                                originImage: metadataReponse?.image,
                                contentType: contentType,
                                metadataUpdatedAt: nowInMillis(),
                                ipfsUpdatedAt: metadataReponse?.image == nftOnchain.originImage ? nowInMillis() : nowInMillis() - 1000000000000,
                                isIpfsUpdated: metadataReponse?.image == nftOnchain.originImage ? true : false,
                                isMetadataUpdated: true,
                            });
                            logger.info(`MetaDataWorkerService::crawlData end get MetaData nftOnchain id: ${nftOnchain.id}`);
                            return true;
                        } catch (ex) {
                            logger.error(ex);
                            try {
                                await manager.getRepository(NftOnchain).update({id: nftOnchain.id}, {
                                    metadataUpdatedAt: nowInMillis(),
                                    isMetadataUpdated: false,
                                });
                            } catch (ex2) {
                            }
                        }
                    })
                }));

            return true;
        });
    }
}
