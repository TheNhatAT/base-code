import {AuctionStatus, LootBoxStatus} from '../../shared/enums';
import {Config, User} from '../../database/entities';
import {LatestBlock} from '../../database/entities';
import {NftOwner} from '../../database/entities';
import {Injectable} from '@nestjs/common';
import * as _ from 'lodash';
import {InjectDataSource, InjectRepository} from '@nestjs/typeorm';
import {
    Collection,
    CurrencyConfig,
} from '../../database/entities';
import {getLogger} from '../../shared/logger';
import {DataSource, Repository} from 'typeorm';
import {NotificationService} from '../notification/notification.service';
import {CrawlWorkerService} from './crawl-worker.service';
import {SocketService} from './socket.service';
import {MetaDataWorkerService} from './metadata-worker.service';
import {CollectionStatus} from 'src/shared/enums';
import {S3Handler} from 'src/shared/S3Handler';
import {IPFSWorkerService} from './ipfs-worker.service';
import {KmsService} from "../common/kms.service";
import {AddressesService} from "../addresses/addresses.service";

const logger = getLogger('WorkerManagerService');
var cron = require('node-cron');

@Injectable()
export class WorkerManagerService {

    private _collectionMapping = {}

    constructor(
        private readonly kmsService: KmsService,
        private readonly notificationService: NotificationService,
        private readonly socketService: SocketService,
        private readonly addressesService: AddressesService,
        @InjectDataSource()
        private dataSource: DataSource,
        @InjectRepository(CurrencyConfig)
        private currenciesRepository: Repository<CurrencyConfig>,
        @InjectRepository(Collection)
        private collectionRepository: Repository<Collection>,
        @InjectRepository(NftOwner)
        private readonly nftOwnerRepository: Repository<NftOwner>,
        @InjectRepository(LatestBlock)
        private readonly latestBlockRepository: Repository<LatestBlock>,
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        @InjectRepository(Config)
        private readonly configRepository: Repository<Config>,

        private readonly s3handler: S3Handler
    ) {
        this.init();
        cron.schedule('* * * * *', async () => {
            await this.syncCollectionData();
        });
    }

    async init() {
        new MetaDataWorkerService(this.dataSource);
        new IPFSWorkerService(this.s3handler, this.dataSource);

        await this.syncCollectionData();
    }

    runWorker(_cb: () => void) {
        try {
            _cb();
        } catch (error) {
            logger.error(error);
        }
    }

    async syncCollectionData() {
        let currencies = await this.currenciesRepository.find();
        for (let currency of currencies) {
            let collections = await this.collectionRepository.find({
                where: {
                    chainId: currency.chainId
                }
            });
            for (let collection of collections) {
                if (collection.status == CollectionStatus.LISTED || collection.status == CollectionStatus.DELISTED) {
                    const key = "" + currency.chainId + "_" + collection.address;
                    let oldWorker = this._collectionMapping[key];
                    if (!oldWorker || oldWorker.isStopped)  {
                        let worker = new CrawlWorkerService(currency, collection, this.notificationService, this.socketService, this.dataSource);
                        this._collectionMapping[key] = worker;
                    }
                    logger.info(`WorkerManagerService::syncCollectionData Collection ${collection.name} is ${collection.status}`);
                    this._collectionMapping[key].isDelisted = collection.status == CollectionStatus.DELISTED;
                }
            }
        }
    }



}
