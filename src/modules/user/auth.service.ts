import {Injectable} from '@nestjs/common';
import {JwtService} from '@nestjs/jwt';
import {RegisterWallet} from './request/register-wallet.dto';
import {ExternalWallet} from './request/external-wallet.dto';
import {LoginResponse} from './response/login.dto';
import * as argon2 from 'argon2';
import {Admin, Countries, MetaData, NftOnchain, User, UserBrand} from '../../database/entities';
import {InjectDataSource, InjectRepository} from '@nestjs/typeorm';
import {DataSource, Repository} from 'typeorm';
import {Register} from './request/register.dto';
import {convertToObject, encrypt, randomNumberCode} from '../../shared/Utils';
import {MailService} from '../mail/mail.service';
import {AddressesService} from '../addresses/addresses.service';
import {S3} from 'aws-sdk';
import {IPaginationOptions} from 'nestjs-typeorm-paginate';
import {PaginationResponse} from 'src/config/rest/paginationResponse';
import {getArrayPaginationBuildTotal, getOffset} from 'src/shared/Utils';
import {ApiCauses} from '../../config/exception/apiCauses';
import {Collection} from "../../database/entities/Collection.entity";

var tokenMap = new Map();
var limitRequestLoginMap = new Map();

@Injectable()
export class AuthService {
    constructor(
        private jwtService: JwtService,
        private readonly mailService: MailService,
        private readonly addressesService: AddressesService,
        @InjectDataSource()
        private dataSource: DataSource,
        @InjectRepository(User)
        private usersRepository: Repository<User>,
        @InjectRepository(UserBrand)
        private userBrandRepository: Repository<UserBrand>,
        @InjectRepository(Countries)
        private countriesRepository: Repository<Countries>,
        @InjectRepository(Admin)
        private adminRepository: Repository<Admin>,
    ) {
    }

    //login
    async validateUser(data: any): Promise<any> {
        const user = await this.getUserByEmail(data.email);
        if (user && user.email && user.password && user.status && user.status == 'active') {
            const key = encrypt('Login-' + user.email);
            let dataCheck = limitRequestLoginMap.get(key) ? limitRequestLoginMap.get(key) : {};

            if (dataCheck.total && dataCheck.total >= (parseInt(process.env.LIMIT_REQUEST) || 5)) {
                if (dataCheck.timeRequest && Date.now() - dataCheck.timeRequest < (parseInt(process.env.LIMIT_HOURS_BLOCK_REQUEST) || 4) * 60 * 60 * 1000) return null;

                dataCheck.total = 0;
                dataCheck.timeRequest = Date.now();
                limitRequestLoginMap.set(key, dataCheck);
            }

            //verify hashed password and plain-password
            const isPassword = await argon2.verify(user.password, data.password);

            if (isPassword) {
                if (dataCheck.total) {
                    limitRequestLoginMap.delete(key);
                }

                const {password, ...result} = user;
                return result;

            } else {
                if (dataCheck.total) {
                    dataCheck.total += 1;
                } else {
                    dataCheck.total = 1;
                }
                dataCheck.timeRequest = Date.now();
                limitRequestLoginMap.set(key, dataCheck);
            }
        }
        return null;
    }

    async validateUserEmailBlackList(email: any) {
        let user = await this.getUserByEmail(email);

        return this.validateUserBlackList(user)
    }

    async validateUserBlackList(user: User) {
        if (user) {
            if (user.group == 1) {
                return false;
            } else {
                return true;
            }
        }

        return true;
    }

    isValidToken(token: string) {
        // return tokenMap.get(encrypt(token)) == '1';
        return this.jwtService.verify(token);
    }

    setValidToken(token: string) {
        tokenMap.set(encrypt(token), '1');
    }

    deleteValidToken(token: string) {
        tokenMap.delete(encrypt(token));
    }

    async login(user: any): Promise<LoginResponse> {
        const payload = {username: user.username, userId: user.id};
        const tokenLogin = this.jwtService.sign(payload);

        // this.setValidToken(tokenLogin);

        const {password, token, twoFactorAuthenticationSecret, ...dataReturn} = user;

        return {
            ...dataReturn,
            token: tokenLogin
        };
    }


    async getCountriesList(paginationOptions: IPaginationOptions): Promise<PaginationResponse<Countries>> {

        let offset = getOffset(paginationOptions);
        let limit = Number(paginationOptions.limit);
        let queryBuilder = this.dataSource
            .createQueryBuilder(Countries, 'countries')
            .select("countries.id, countries.code,countries.name")
            .orderBy("countries.id", "ASC")
            .limit(limit)
            .offset(offset);
        let queryCount = this.dataSource
            .createQueryBuilder(Countries, 'countries')
            .select(" Count (1) as Total")
            .orderBy("countries.id", "ASC");

        const countries = await queryBuilder.execute();
        const countriesCountList = await queryCount.execute();


        const {items, meta} = getArrayPaginationBuildTotal<Countries>(countries, countriesCountList, paginationOptions);

        return {
            results: items,
            pagination: meta,
        };
    }

    async getListAddressBook(paginationOptions: IPaginationOptions): Promise<PaginationResponse<User>> {

        let offset = getOffset(paginationOptions);
        let limit = Number(paginationOptions.limit);
        let queryBuilder = this.dataSource
            .createQueryBuilder(User, 'user')
            .select("user.id, user.username,user.wallet")
            .orderBy("user.id", "ASC")
            .limit(limit)
            .offset(offset)
            .where("user.wallet is not null");
        let queryCount = this.dataSource
            .createQueryBuilder(User, 'user')
            .select(" Count (1) as Total")
            .where("user.wallet is not null");

        const users = await queryBuilder.execute();
        const usersCountList = await queryCount.execute();


        const {items, meta} = getArrayPaginationBuildTotal<User>(users, usersCountList, paginationOptions);

        return {
            results: items,
            pagination: meta,
        };
    }

    async getListVender(paginationOptions: IPaginationOptions): Promise<PaginationResponse<User>> {

        let offset = getOffset(paginationOptions);
        let limit = Number(paginationOptions.limit);
        let queryBuilder = this.dataSource
            .createQueryBuilder(User, 'user')
            .select("user.id, user.username,user.wallet, user.vendor_name")
            .orderBy("user.id", "ASC")
            .limit(limit)
            .offset(offset)
            .where("user.wallet is not null and user.is_vendor = true");
        let queryCount = this.dataSource
            .createQueryBuilder(User, 'user')
            .select(" Count (1) as Total")
            .where("user.wallet is not null and user.is_vendor = true");

        const users = await queryBuilder.execute();
        const usersCountList = await queryCount.execute();


        const {items, meta} = getArrayPaginationBuildTotal<User>(users, usersCountList, paginationOptions);

        return {
            results: items,
            pagination: meta,
        };
    }


    async getUserById(id: number): Promise<User | undefined> {
        let queryDataUser = this.dataSource
            .createQueryBuilder(User, 'user')
            .leftJoin(Countries, 'countries', 'countries.id = user.country_id')
            .select("user.id, user.username, user.email, user.avatar_url as avatarUrl, user.first_name as firstName, user.last_name as lastName, user.date_of_birth as dateOfBirth, user.phone, user.created_at as createdAt, user.updated_at as updatedAt, user.is_active_2fa as isActive2fa, user.is_active_email_code as isActiveEmailCode, user.two_factor_authentication_secret as twoFactorAuthenticationSecret, user.email_code as emailCode, user.is_active_kyc as isActiveKyc, user.wallet, user.status, user.type, user.token,user.brand_id as brandId,user.group,user.vendor_name as vendorName,user.is_vendor as isVendor")
            .addSelect("countries.id as country_id, countries.code, countries.name")
            .where('user.id  = :Id', {Id: id});

        const user = await queryDataUser.execute();

        if (user && user.length > 0) {
            let queryGetBrand = this.dataSource
                .createQueryBuilder(UserBrand, 'user_brand')
                .select(" user_brand.brand_id")
                .addSelect("brand.id, brand.brand_name")
                .where('user_brand.user_id =  :userId', {userId: id});

            const brands = await queryGetBrand.execute();
            let listBrands = []
            if (brands && brands.length > 0) {
                for (let item in brands) {
                    const dataInsert = {
                        id: brands[item].id,
                        brandName: brands[item].brand_name,
                    }

                    listBrands.push(dataInsert)
                }
            }

            user[0].brandIds = listBrands;
            return user[0];
        }
    }

    async getDetailUser(id: number) {
        let queryBuilder = this.dataSource
            .createQueryBuilder(User, 'user')
            .select("user.id, user.username, user.email, user.avatar_url as avatarUrl, user.first_name as firstName, user.last_name as lastName, user.date_of_birth as dateOfBirth, user.phone, user.created_at as createdAt, user.updated_at as updatedAt, user.is_active_2fa as isActive2fa, user.is_active_email_code as isActiveEmailCode, user.two_factor_authentication_secret as twoFactorAuthenticationSecret, user.email_code as emailCode, user.is_active_kyc as isActiveKyc, user.wallet, user.status, user.type, user.token,user.brand_id as brandId,user.group, user.vendor_name as vendorName,user.is_vendor as isVendor")
            .addSelect("brand.brand_name as brandName")
            .where('user.id  = :Id', {Id: id});
        const user = await queryBuilder.execute();


        if (user && user.length > 0) {
            let queryCount = this.dataSource
                .createQueryBuilder(NftOnchain, 'nft_onchain')
                .select(" Count (1) as Total")
                .where('nft_onchain.human_owner = :humanOwner', {humanOwner: user[0].wallet})

            const countNFT = await queryCount.execute();
            user[0].countNFT = countNFT[0].Total;
            return user[0];
        }
        return [];
    }

    async getUserByEmail(email: string): Promise<User | undefined> {
        return this.usersRepository.findOne({
            where: {
                email: email,
            }
        });
    }

    async getDetailUserV2(id: number) {
        let queryBuilder = this.dataSource
            .createQueryBuilder(User, 'user')
            .leftJoin(UserBrand, 'user_brand', 'user_brand.user_id = user.id')
            .select("user.id, user.username, user.email, user.avatar_url as avatarUrl, user.first_name as firstName, user.last_name as lastName, user.date_of_birth as dateOfBirth, user.phone, user.created_at as createdAt, user.updated_at as updatedAt, user.is_active_2fa as isActive2fa, user.is_active_email_code as isActiveEmailCode, user.two_factor_authentication_secret as twoFactorAuthenticationSecret, user.email_code as emailCode, user.is_active_kyc as isActiveKyc, user.wallet, user.status, user.type, user.token,user.brand_id as brandId,user.group, user.vendor_name, user.is_vendor")
            .addSelect("count(brand.id) as countBrand")
            .where('user.id  = :Id', {Id: id})
            .groupBy("user.id, user.username, user.email, user.avatar_url, user.first_name, user.last_name, user.date_of_birth, user.phone, user.created_at, user.updated_at, user.is_active_2fa, user.is_active_email_code, user.two_factor_authentication_secret, user.email_code, user.is_active_kyc, user.wallet, user.status, user.type, user.token,user.brand_id,user.group");
        const user = await queryBuilder.execute();


        if (user && user.length > 0) {
            let queryCount = this.dataSource
                .createQueryBuilder(NftOnchain, 'nft_onchain')
                .select(" Count (1) as Total")
                .where('nft_onchain.human_owner = :humanOwner', {humanOwner: user[0].wallet})

            const countNFT = await queryCount.execute();
            user[0].countNFT = countNFT[0].Total;

            // get brand data
            let queryGetBrand = this.dataSource
                .createQueryBuilder(UserBrand, 'user_brand')
                .select(" user_brand.brand_id")
                .addSelect("brand.id, brand.brand_name")
                .where('user_brand.user_id =  :userId', {userId: id});

            const dataBrand = await queryGetBrand.execute();

            // new array
            let listBrand = [];
            for (let item in dataBrand) {
                let queryCountNFt = this.dataSource
                    .createQueryBuilder(NftOnchain, 'nft_onchain')
                    .leftJoin(MetaData, 'meta_data', 'nft_onchain.token_uri = meta_data.id')
                    .leftJoin(Collection, 'collection', 'collection.id = meta_data.collection_id')
                    .select(" Count (1) as Total")
                    .where('nft_onchain.human_owner = :humanOwner and collection.brand_id = :brandId', {
                        humanOwner: user[0].wallet,
                        brandId: dataBrand[item].id
                    })
                const countNFTByBrand = await queryCountNFt.execute();
                const dataInsert = {
                    id: dataBrand[item].id,
                    brandName: dataBrand[item].brand_name,
                    totalNFT: countNFTByBrand[0].Total,
                };
                listBrand.push(dataInsert);
            }
            user[0].listBrand = listBrand;
            return user[0];
        }
        return [];
    }

    async getUserByUsername(username: string): Promise<User | undefined> {
        return this.usersRepository.findOne({
            where: {
                username: username
            }
        });
    }

    async getUserByWallet(wallet: string): Promise<User | undefined> {
        return this.usersRepository.findOne({
            where: {
                wallet: wallet
            }
        });
    }

    async getUserByData(data: any): Promise<User | undefined> {
        let user = null;

        if (data.email) {
            user = this.usersRepository.findOne({
                where: {
                    email: data.email
                }
            });
        }

        if (data.username) {
            user = this.usersRepository.findOne({
                where: {
                    username: data.username
                }
            });
        }

        if (data.address) {
            user = this.usersRepository.findOne({
                where: {
                    wallet: data.address
                }
            });
        }

        return user;

    }

    async createUserByWallet(data: RegisterWallet): Promise<any> {
        let user = new User();
        user.email = data.email;
        user.username = data.username;
        user.wallet = data.address;
        // user.status = "active";
        user.status = "request";
        user.type = "wallet"

        user = await this.usersRepository.save(user);
        const tokenUser = await this.getToken(user);
        const urlActive = process.env.URL_API + 'user/active' + '?code=' + tokenUser;
        const content = "To activate your account, please click on the link below: " + urlActive;
        const subject = "Confirm email for active account";

        this.mailService.sendMail(user.email, subject, content);

        const {password, token, twoFactorAuthenticationSecret, ...dataUser} = user;

        return dataUser;
    }

    async connectExternalWallet(data: ExternalWallet, userData: User): Promise<any> {
        if (!userData || !userData.username) return false;
        let user = await this.getUserByUsername(userData.username);
        if (user.wallet) {
            throw ApiCauses.DUPLICATED_EMAIL_OR_USERNAME;
        } else {
            user.wallet = data.address;
            user.type = "wallet";
            user = await this.usersRepository.save(user);
            return user;
        }
    }

    async connectVerdantWallet(userData: User): Promise<any> {
        if (!userData || !userData.username) return false;
        let user = await this.getUserByUsername(userData.username);
        if (user.wallet) {
            throw ApiCauses.DUPLICATED_EMAIL_OR_USERNAME;
        } else {

            // await this.dataSource.transaction(async (transactional) => {
            //     user.wallet = (await this.addressesService.generateAddress(transactional, user.email)).address;
            //     user = await transactional.save(user);
            // });

            return user;
        }

    }

    //register
    async checkDuplicatedUser(data: Register): Promise<any> {
        //check duplicated username or email
        const duplicatedUser = await this.getUserByEmail(data.email);
        return duplicatedUser;
    }

    async updateProfile(user: User, data: any) {
        if (!user || !user.username || !data) return false;

        let dataUser = await this.getUserByUsername(user.username);

        if (!dataUser) return false;

        for (const [key, value] of Object.entries(data)) {
            if (['firstName', 'lastName', 'phone', 'dateOfBirth', 'wallet'].includes(key)) {
                dataUser[key] = value;
            }
        }

        dataUser = await this.usersRepository.save(dataUser);

        const {password, token, twoFactorAuthenticationSecret, ...dataReturn} = dataUser;

        return dataReturn;
    }

    async updateProfileFile(user: User, data: any, file: any) {
        //if (!user || !user.username || !data) return false;
        if (!user || !user.username) return false;
        let dataUser = await this.getUserByUsername(user.username);

        if (!dataUser) return false;
        if (data) {
            for (const [key, value] of Object.entries(data)) {
                if (['firstName', 'lastName', 'phone', 'dateOfBirth', 'wallet', 'countryId'].includes(key)) {
                    dataUser[key] = value;
                }
            }

        } else {
            dataUser.firstName = null;
            dataUser.lastName = null;
            dataUser.phone = null;
            dataUser.dateOfBirth = null;
        }
        if (file) {
            const upload = await this.upload(file);


            if (!upload || !upload.Location) return false;
            dataUser.avatarUrl = upload.Location;
        }

        dataUser = await this.usersRepository.save(dataUser);

        const {password, token, twoFactorAuthenticationSecret, ...dataReturn} = dataUser;

        return dataReturn;
    }

    async updatePassword(user: User, data: any) {
        if (!user || !user.username || !data) return false;
        let dataUser = await this.getUserByUsername(user.username);
        if (!dataUser) return false;
        if (!dataUser.password) {
            throw ApiCauses.NO_CHANGE_PASS;
        }
        const isPassword = await argon2.verify(dataUser.password, data.oldPassword);
        if (!isPassword)
            throw ApiCauses.PASSWORD_IS_FALSE;

        const hashedNewPassword = await argon2.hash(data.newPassword);

        dataUser.password = hashedNewPassword;
        dataUser = await this.usersRepository.save(dataUser);

        const {password, token, twoFactorAuthenticationSecret, ...dataReturn} = dataUser;

        return dataReturn;
    }

    async activeUser(token: string) {
        if (!token) return false;

        let user = await this.getUserByToken(token);

        if (!user || user.status !== 'request') return false;

        user.status = 'active';

        user = await this.usersRepository.save(user);

        return user;
    }

    async resetPassword(dataToken: string, dataPassword: string) {
        if (!dataToken) return false;


        const data = convertToObject(this.jwtService.decode(dataToken));

        if (!data || !data.time || (Date.now() - data.time) > parseInt(process.env.EXPRIRE_TIME_EMAIL_CODE)) return false;


        let user = await this.getUserByToken(dataToken);

        if (!user) return false;

        const hashedPassword = await argon2.hash(dataPassword);
        user.password = hashedPassword;
        user.token = null;
        user = await this.usersRepository.save(user);

        const {password, token, twoFactorAuthenticationSecret, ...dataReturn} = user;

        return dataReturn;
    }

    async registerUser(data: Register): Promise<any> {
        //hash password
        const hashedPassword = await argon2.hash(data.password);

        //insert user table
        const user = await this._registerUser(data.email, hashedPassword);

        const token = await this.getToken(user);

        // send mail active
        const urlActive = process.env.URL_API + 'user/active' + '?code=' + token;
        const content = "To activate your account, please click on the link below: " + urlActive;
        const subject = "Confirm email for active account";

        this.mailService.sendMail(user.email, subject, content);

        return {
            email: user.email
        };
    }

    async registerUserV2(data: Register): Promise<any> {
        //hash password
        const hashedPassword = await argon2.hash(data.password);

        //insert user table
        const user = await this._registerUserV2(data.email, hashedPassword);

        const token = await this.getToken(user);

        // send mail active
        const urlActive = process.env.URL_API + 'user/active' + '?code=' + token;
        const content = "To activate your account, please click on the link below: " + urlActive;
        const subject = "Confirm email for active account";

        this.mailService.sendMail(user.email, subject, content);

        return {
            email: user.email
        };
    }

    async resendMailActiveUser(email: string): Promise<any> {

        let user = await this.getUserByEmail(email);

        if (!user || user.status !== 'request') return false;

        const token = await this.getToken(user);

        const urlActive = process.env.URL_API + 'user/active' + '?code=' + token;
        const content = "To activate your account, please click on the link below: " + urlActive;
        const subject = "Confirm email for active account";

        this.mailService.sendMail(user.email, subject, content);

        return {
            email: user.email
        };

    }

    async sendMailResetPassword(email: string): Promise<any> {

        let user = await this.getUserByEmail(email);

        if (!user) return false;

        const token = await this.getToken(user);

        // send mail active
        const urlActive = process.env.URL_FRONTEND + 'reset-password' + '?code=' + token;
        const content = "To update password your account, please click on the link below: " + urlActive;
        const subject = "Confirm email for reset password";

        this.mailService.sendMail(user.email, subject, content);

        return {
            email: user.email
        }
    }

    async sendEmailCode(user: User): Promise<any> {

        const code = await this.getEmailCode(user);

        // send mail active
        const content = "Your email code: " + code;
        const subject = "Please input to website";

        this.mailService.sendMail(user.email, subject, content);

        return {
            email: user.email
        }
    }

    async _registerUser(email: string, password: string) {
        let user = new User();
        await this.dataSource.transaction(async (transactional) => {

            user.email = email;
            user.username = email;
            user.password = password;
            // user.wallet = (await this.addressesService.generateAddress(transactional, user.email)).address;
            user.type = 'email';

            user = await transactional.save(user);
        });

        return user;
    }

    async _registerUserV2(email: string, password: string) {
        let user = new User();
        await this.dataSource.transaction(async (transactional) => {

            user.email = email;
            user.username = email;
            user.password = password;
            user.type = 'email';
            user.status = 'request';
            user = await transactional.save(user);
        });

        return user;
    }

    async getToken(user: User) {
        const token = this.jwtService.sign({username: user.username, time: Date.now()});

        user.token = token;
        user = await this.usersRepository.save(user);

        return token;
    }

    async getEmailCode(user: User) {
        const code = randomNumberCode(6);
        const emailCode = this.jwtService.sign({code, time: Date.now()});

        user.emailCode = emailCode;
        user = await this.usersRepository.save(user);

        return code;
    }

    async checkEmailCode(user: User, code: string) {
        if (!user || !user.emailCode) return false;

        const data = convertToObject(this.jwtService.decode(user.emailCode));


        if (!data || !data.code || !data.time || (Date.now() - data.time) > parseInt(process.env.EXPRIRE_TIME_EMAIL_CODE)) return false;

        if (data.code !== code) return false;

        return true;
    }

    async updateStatusEmailCode(user: User, status) {

        user.isActiveEmailCode = status;
        user = await this.usersRepository.save(user);

        return user.isActiveEmailCode;
    }

    async updateBrandToUser(data: any) {
        let user = await this.usersRepository.findOne(data.id);
        if (user) {
            if (data.brandId) {
                user.brandId = data.brandId;
            } else {
                user.brandId = null;
            }
        }
        user = await this.usersRepository.save(user);
        return user;
    }

    async updateBrandToUserV2(data: any) {


        const userBrand = await this.userBrandRepository.findOne({
            where: {
                userId: data.id
            }
        });
        if (userBrand) {
            this.userBrandRepository.createQueryBuilder("user_brand")
                .select("user_brand.id")
                .where("user_brand.user_id = :id", {id: data.id})
                .delete()
                .execute();
        }

        if (data.brandIds && data.brandIds.length > 0) {
            for (let item in data.brandIds) {
                let user = new UserBrand();
                user.brandId = data.brandIds[item];
                user.userId = data.id;
                await this.userBrandRepository.save(user);
            }
        }
        // Update user 
        let userUpdate = await this.usersRepository.findOne(data.id);
        if (userUpdate) {
            if (data.vendorName) {
                userUpdate.vendorName = data.vendorName;
                userUpdate.isVendor = true;
            }
        }
        await this.usersRepository.save(userUpdate);
        // add admin
        const token = this.jwtService.sign({username: userUpdate.username, time: Date.now()});


        let adminData = await this.adminRepository.findOne({
            where: {
                email: userUpdate.email
            }
        });
        if (adminData) {
            let admin = new Admin();
            admin.email = userUpdate.email;
            admin.fullName = userUpdate.lastName + userUpdate.firstName;
            admin.password = userUpdate.password;
            admin.username = userUpdate.username;
            await this.adminRepository.save(admin);
        }


        // Select
        let queryBuilder = this.dataSource
            .createQueryBuilder(User, 'user')
            .leftJoin(UserBrand, 'user_brand', 'user_brand.user_id = user.id')
            .select("user.id, user.username, user.email, user.avatar_url as avatarUrl, user.first_name as firstName, user.last_name as lastName, user.date_of_birth as dateOfBirth, user.phone, user.created_at as createdAt, user.updated_at as updatedAt, user.is_active_2fa as isActive2fa, user.is_active_email_code as isActiveEmailCode, user.two_factor_authentication_secret as twoFactorAuthenticationSecret, user.email_code as emailCode, user.is_active_kyc as isActiveKyc, user.wallet, user.status, user.type, user.token,user.brand_id as brandId,user.group")
            .addSelect("count(brand.id) as countBrand")
            .orderBy("user.updated_at", "DESC")
            .where("user.id = :id", {id: data.id})
            .groupBy("user.id, user.username, user.email, user.avatar_url, user.first_name, user.last_name, user.date_of_birth, user.phone, user.created_at, user.updated_at, user.is_active_2fa, user.is_active_email_code, user.two_factor_authentication_secret, user.email_code, user.is_active_kyc, user.wallet, user.status, user.type, user.token,user.brand_id,user.group");


        const user = await queryBuilder.execute();
        return user;
    }

    async getUserByToken(token: string) {
        const data = convertToObject(this.jwtService.decode(token));

        if (!data || !data.username || !data.time || (Date.now() - data.time) > parseInt(process.env.EXPRIRE_TIME_TOKEN)) return false;

        let user = await this.getUserByUsername(data.username);

        if (!user || !user.token || user.token !== token) return false;

        return user;
    }


    logout(token: string) {
        const tokenWithoutBearer = token.split(' ')[1];

        this.deleteValidToken(tokenWithoutBearer);
    }

    async upload(file): Promise<any> {
        const {originalname} = file;
        const bucketS3 = process.env.AWS_BUCKET;
        return await this.uploadS3(file.buffer, bucketS3, originalname);
    }

    async uploadS3(file, bucket, name) {
        const s3 = this.getS3();
        const params = {
            Bucket: bucket,
            Key: 'users/' + String(name),
            Body: file,
        };
        return new Promise((resolve, reject) => {
            s3.upload(params, (err, data) => {
                if (err) {
                    console.log('S3 eror: ', err);
                    reject(err.message);
                }
                return resolve(data);
            });
        });
    }

    getS3() {
        return new S3({
            accessKeyId: process.env.AWS_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        });
    }

    async getList(params, paginationOptions: IPaginationOptions): Promise<PaginationResponse<User>> {

        let offset = getOffset(paginationOptions);
        let limit = Number(paginationOptions.limit);
        let queryBuilder = this.dataSource
            .createQueryBuilder(User, 'user')
            .select("user.id, user.username, user.email, user.avatar_url as avatarUrl, user.first_name as firstName, user.last_name as lastName, user.date_of_birth as dateOfBirth, user.phone, user.created_at as createdAt, user.updated_at as updatedAt, user.is_active_2fa as isActive2fa, user.is_active_email_code as isActiveEmailCode, user.two_factor_authentication_secret as twoFactorAuthenticationSecret, user.email_code as emailCode, user.is_active_kyc as isActiveKyc, user.wallet, user.status, user.type, user.token,user.brand_id as brandId,user.group,user.vendor_name as vendorName,user.is_vendor as isVendor")
            .addSelect("brand.brand_name as brand_name, brand.id  as brand_id")
            .orderBy("user.updated_at", "DESC")
            .limit(limit)
            .offset(offset);
        let queryCount = this.dataSource
            .createQueryBuilder(User, 'user')
            .select(" Count (1) as Total")
            .orderBy("user.updated_at", "DESC");
        if (params.username) {
            queryBuilder.andWhere(`user.username like '%${params.username.trim()}%'`);
            queryCount.andWhere(`user.username like '%${params.username.trim()}%'`);
        }

        if (params.email) {
            queryBuilder.andWhere(`user.email like '%${params.email.trim()}%'`);
            queryCount.andWhere(`user.email like '%${params.email.trim()}%'`);
        }

        if (params.wallet) {
            queryBuilder.andWhere(`user.wallet like '%${params.wallet.trim()}%'`);
            queryCount.andWhere(`user.wallet like '%${params.wallet.trim()}%'`);
        }

        if (params.status) {
            queryBuilder.andWhere(`user.status like '%${params.status.trim()}%'`);
            queryCount.andWhere(`user.status like '%${params.status.trim()}%'`);
        }
        const users = await queryBuilder.execute();
        const usersCountList = await queryCount.execute();


        const {items, meta} = getArrayPaginationBuildTotal<User>(users, usersCountList, paginationOptions);

        return {
            results: items,
            pagination: meta,
        };
    }

    async getListV2(params, paginationOptions: IPaginationOptions): Promise<PaginationResponse<User>> {

        let offset = getOffset(paginationOptions);
        let limit = Number(paginationOptions.limit);
        let queryBuilder = this.dataSource
            .createQueryBuilder(User, 'user')
            .leftJoin(UserBrand, 'user_brand', 'user_brand.user_id = user.id')
            .select("user.id, user.username, user.email, user.avatar_url as avatarUrl, user.first_name as firstName, user.last_name as lastName, user.date_of_birth as dateOfBirth, user.phone, user.created_at as createdAt, user.updated_at as updatedAt, user.is_active_2fa as isActive2fa, user.is_active_email_code as isActiveEmailCode, user.two_factor_authentication_secret as twoFactorAuthenticationSecret, user.email_code as emailCode, user.is_active_kyc as isActiveKyc, user.wallet, user.status, user.type, user.token,user.brand_id as brandId,user.group,user.vendor_name as vendorName,user.is_vendor as isVendor")
            .addSelect("count(brand.id) as countBrand")
            .addSelect("(case when whitelist_minter.status then true else false end) as minter")
            .orderBy("user.updated_at", "DESC")
            .groupBy("user.id, user.username, user.email, user.avatar_url, user.first_name, user.last_name, user.date_of_birth, user.phone, user.created_at, user.updated_at, user.is_active_2fa, user.is_active_email_code, user.two_factor_authentication_secret, user.email_code, user.is_active_kyc, user.wallet, user.status, user.type, user.token,user.brand_id,user.group,whitelist_minter.status")
            .limit(limit)
            .offset(offset);
        let queryCount = this.dataSource
            .createQueryBuilder(User, 'user')
            .select(" Count (1) as Total")
            .orderBy("user.updated_at", "DESC");
        if (params.username) {
            queryBuilder.andWhere(`user.username like '%${params.username.trim()}%'`);
            queryCount.andWhere(`user.username like '%${params.username.trim()}%'`);
        }

        if (params.email) {
            queryBuilder.andWhere(`user.email like '%${params.email.trim()}%'`);
            queryCount.andWhere(`user.email like '%${params.email.trim()}%'`);
        }

        if (params.wallet) {
            queryBuilder.andWhere(`user.wallet like '%${params.wallet.trim()}%'`);
            queryCount.andWhere(`user.wallet like '%${params.wallet.trim()}%'`);
        }

        if (params.status) {
            queryBuilder.andWhere(`user.status like '%${params.status.trim()}%'`);
            queryCount.andWhere(`user.status like '%${params.status.trim()}%'`);
        }
        const users = await queryBuilder.execute();
        const usersCountList = await queryCount.execute();


        const {items, meta} = getArrayPaginationBuildTotal<User>(users, usersCountList, paginationOptions);

        return {
            results: items,
            pagination: meta,
        };
    }

    async getBlackList(params, paginationOptions: IPaginationOptions): Promise<PaginationResponse<User>> {

        let offset = getOffset(paginationOptions);
        let limit = Number(paginationOptions.limit);
        let queryBuilder = this.dataSource
            .createQueryBuilder(User, 'user')
            .select("user.id, user.username, user.email, user.avatar_url, user.first_name, user.group, user.last_name, user.phone, user.wallet, user.created_at, user.updated_at")
            .orderBy("user.updated_at", "DESC")
            .limit(limit)
            .offset(offset)
            .where('user.group = 1');
        let queryCount = this.dataSource
            .createQueryBuilder(User, 'user')
            .select(" Count (1) as Total")
            .where('user.group = 1');
        if (params.name) {
            queryBuilder.andWhere(`user.username like '%${params.name.trim()}%'`);
            queryCount.andWhere(`user.username like '%${params.name.trim()}%'`);
        }
        const users = await queryBuilder.execute();
        const usersCountList = await queryCount.execute();


        const {items, meta} = getArrayPaginationBuildTotal<User>(users, usersCountList, paginationOptions);

        return {
            results: items,
            pagination: meta,
        };
    }

    async addBlackList(wallet) {
        let dataUser = await this.getUserByWallet(wallet);
        if (!dataUser) throw ApiCauses.NO_USER_BY_WALLET;
        if (dataUser.group == 1) {
            throw ApiCauses.USER_IS_BLACKLIST;
        }

        dataUser.group = 1;

        dataUser = await this.usersRepository.save(dataUser);
        return dataUser;
    }

    async removeBlackWhiteList(wallet) {
        let dataUser = await this.getUserByWallet(wallet);
        if (!dataUser) return false;

        dataUser.group = 0;

        dataUser = await this.usersRepository.save(dataUser);
        return dataUser;
    }


    async getWhiteList(params, paginationOptions: IPaginationOptions): Promise<PaginationResponse<User>> {

        let offset = getOffset(paginationOptions);
        let limit = Number(paginationOptions.limit);
        let queryBuilder = this.dataSource
            .createQueryBuilder(User, 'user')
            .select("user.id, user.username, user.email, user.avatar_url, user.first_name, user.group, user.last_name, user.phone, user.wallet, user.created_at, user.updated_at")
            .orderBy("user.updated_at", "DESC")
            .limit(limit)
            .offset(offset)
            .where('user.group = 2');
        let queryCount = this.dataSource
            .createQueryBuilder(User, 'user')
            .select(" Count (1) as Total")
            .where('user.group = 2');
        if (params.name) {
            queryBuilder.andWhere(`user.username like '%${params.name.trim()}%'`);
            queryCount.andWhere(`user.username like '%${params.name.trim()}%'`);
        }

        if (params.address) {
            queryBuilder.andWhere(
                `user.wallet like '%${params.address.trim()}%'`
            );
            queryCount.andWhere(
                `user.wallet like '%${params.address.trim()}%'`
            );
        }


        const users = await queryBuilder.execute();
        const usersCountList = await queryCount.execute();


        const {items, meta} = getArrayPaginationBuildTotal<User>(users, usersCountList, paginationOptions);

        return {
            results: items,
            pagination: meta,
        };
    }

    async addWhiteList(wallet) {
        let dataUser = await this.getUserByWallet(wallet);

        if (!dataUser) return false;

        dataUser.group = 2;

        dataUser = await this.usersRepository.save(dataUser);
        return dataUser;

    }
}
