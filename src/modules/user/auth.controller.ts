import {
    Body,
    Controller,
    DefaultValuePipe,
    Get,
    HttpStatus,
    Post,
    Query,
    Req,
    Res,
    UploadedFile,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import {AuthService} from './auth.service';
import {Login} from './request/login.dto';
import {LoginResponse} from './response/login.dto';
import {EmptyObject} from '../../shared/response/emptyObject.dto';
import {ApiOperation, ApiQuery, ApiResponse} from '@nestjs/swagger';
import {LoginBase} from './response/loginBase.dto';
import {ApiCauses} from '../../config/exception/apiCauses';
import {EmptyObjectBase} from '../../shared/response/emptyObjectBase.dto';
import {JwtAuthGuard} from './jwt-auth.guard';
import {Register} from './request/register.dto';
import {RegisterResponse} from './response/register.dto';
import {RegisterBase} from './response/registerBase.dto';
import {ResetPassword} from './request/reset-password.dto';
import {UpdatePassword} from './request/update-password.dto';
import {TwoFactorAuthenticationService} from './twoFactorAuthentication.service';
import {UsersService} from './user.service';
import RequestWithUser from './requestWithUser.interface';
import {JwtService} from '@nestjs/jwt';
import {RegisterWallet} from './request/register-wallet.dto';
import {ExternalWallet} from './request/external-wallet.dto';
import {LoginWallet} from './request/login-wallet.dto';
import {isPhoneNumber} from '../../shared/Utils';
import {checkImage} from 'src/shared/Utils';
import {FileInterceptor} from '@nestjs/platform-express';
import {Countries, User} from '../../database/entities';
import {PaginationResponse} from 'src/config/rest/paginationResponse';
import * as argon2 from 'argon2';


const Web3 = require("web3");

@Controller('user')
export class AuthController {
    _web3 = new Web3();

    constructor(
        private jwtService: JwtService,
        private readonly twoFactorAuthenticationService: TwoFactorAuthenticationService,
        private readonly usersService: UsersService,
        private authService: AuthService,
    ) {
    }

    @Post('/register')
    @ApiOperation({
        tags: ['auth'],
        operationId: 'register',
        summary: 'Register',
        description: 'Register a new user',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async register(@Body() data: Register): Promise<RegisterResponse | EmptyObject> {

        const duplicatedUser = await this.authService.checkDuplicatedUser(data);
        if (duplicatedUser) {
            throw ApiCauses.DUPLICATED_EMAIL_OR_USERNAME;
        }
        const user = await this.authService.registerUser(data);
        return user;
    }

    @Post('/register-v2')
    @ApiOperation({
        tags: ['auth'],
        operationId: 'register',
        summary: 'Register',
        description: 'Register a new user',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async registerV2(@Body() data: Register): Promise<RegisterResponse | EmptyObject> {

        const duplicatedUser = await this.authService.checkDuplicatedUser(data);
        if (duplicatedUser) {
            throw ApiCauses.DUPLICATED_EMAIL_OR_USERNAME;
        }
        const user = await this.authService.registerUserV2(data);
        return user;
    }

    @Get('/active')
    @ApiOperation({
        tags: ['auth'],
        operationId: 'active',
        summary: 'active',
        description: 'Active a new user',
    })
    @ApiQuery({
        name: 'code',
        required: true,
        type: String,
    })
    async active(@Query('code') code: string, @Res() res) {
        if (!code) throw ApiCauses.DATA_INVALID;

        const activeUser = await this.authService.activeUser(code);

        res.setHeader('Access-Control-Allow-Origin', process.env.URL_FRONTEND);

        if (!activeUser) {
            res.redirect(process.env.URL_FRONTEND);
        } else {
            res.redirect(process.env.URL_FRONTEND + 'login');
        }

        return {};
    }

    @Post('/add-wallet')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['auth'],
        operationId: 'add wallet',
        summary: 'add wallet',
        description: 'add wallet',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async addWallet(@Body() data: any, @Req() request: RequestWithUser): Promise<any | EmptyObject> {
        if (!request || !request.user) throw ApiCauses.USER_NOT_ACCESS;

        if (!data || !data.wallet) throw ApiCauses.DATA_INVALID;

        const user = request.user;
        const userUpdate = await this.authService.updateProfile(user, {wallet: data.wallet});

        if (!userUpdate) throw ApiCauses.DATA_INVALID;

        return userUpdate;
    }


    @Post('/update-profile')
    @UseGuards(JwtAuthGuard)
    @UseInterceptors(FileInterceptor('image'))
    @ApiOperation({
        tags: ['auth'],
        operationId: 'update profile',
        summary: 'update profile',
        description: 'update profile',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async updateProfile(@Body() data: any, @Req() request: RequestWithUser, @UploadedFile() file): Promise<any | EmptyObject> {
        if (!request || !request.user) throw ApiCauses.USER_NOT_ACCESS;

        //if (!data) throw ApiCauses.DATA_INVALID;
        if (data.dateOfBirth == '' || !data.dateOfBirth) {
            data.dateOfBirth = null;
        }

        if (data.phone && !isPhoneNumber(data.phone)) throw ApiCauses.PHONE_INVALID;

        const user = request.user;
        if (file) checkImage(file);

        const userUpdate = await this.authService.updateProfileFile(user, data, file);

        if (!userUpdate) throw ApiCauses.DATA_INVALID;

        return userUpdate;
    }

    @Post('/update-password')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['auth'],
        operationId: 'update profile',
        summary: 'update profile',
        description: 'update profile',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async updatePassword(@Body() data: UpdatePassword, @Req() request: RequestWithUser): Promise<any | EmptyObject> {

        if (!request || !request.user) throw ApiCauses.USER_NOT_ACCESS;

        if (!data.oldPassword || !data.newPassword) throw ApiCauses.DATA_INVALID;

        if (data.oldPassword === data.newPassword) throw ApiCauses.DUPLICATE_PASSWORD;

        const user = request.user;
        const userUpdate = await this.authService.updatePassword(user, data);

        if (!userUpdate) throw ApiCauses.DATA_INVALID;

        return userUpdate;
    }

    @Post('/reset-password')
    @ApiOperation({
        tags: ['auth'],
        operationId: 'reset password',
        summary: 'reset password',
        description: 'reset password',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async resetPassword(@Body() data: ResetPassword): Promise<any | EmptyObject> {

        if (!data.password || !data.token) throw ApiCauses.DATA_INVALID;

        const userUpdate = await this.authService.resetPassword(data.token, data.password);

        if (!userUpdate) throw ApiCauses.DATA_INVALID;

        return userUpdate;
    }

    @Post('/resend-mail-active-user')
    @ApiOperation({
        tags: ['auth'],
        operationId: 'reset password',
        summary: 'reset password',
        description: 'reset password',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async resendMailActive(@Body() data: any): Promise<any | EmptyObject> {

        if (!data.email) throw ApiCauses.DATA_INVALID;

        const userUpdate = await this.authService.resendMailActiveUser(data.email);

        if (!userUpdate) throw ApiCauses.USER_ERROR;

        return userUpdate;
    }

    @Post('/send-mail-reset-password')
    @ApiOperation({
        tags: ['auth'],
        operationId: 'reset password',
        summary: 'reset password',
        description: 'reset password',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async sendMailResetPassword(@Body() data: any): Promise<any | EmptyObject> {

        if (!data.email) throw ApiCauses.DATA_INVALID;

        const userUpdate = await this.authService.sendMailResetPassword(data.email);

        if (!userUpdate) throw ApiCauses.DATA_INVALID;

        return userUpdate;
    }

    @Post('/send-email-code')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['auth'],
        operationId: 'send email code',
        summary: 'send email code',
        description: 'send email code',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async sendEmailCode(@Body() data: any, @Req() request: RequestWithUser): Promise<any | EmptyObject> {

        const userRequest = request.user;

        const user = await this.authService.getUserByUsername(userRequest.username);

        if (!user || !user.email) throw ApiCauses.DATA_INVALID;

        try {
            await this.authService.sendEmailCode(user);

            return {};
        } catch (error) {
            console.log('sendEmailCode error: ', error);
            throw ApiCauses.INTERNAL_ERROR;
        }
    }

    @Post('/update-status-email-code')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['auth'],
        operationId: 'update status email code',
        summary: 'update status email code',
        description: 'update status email code',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async updateStatusEmailCode(@Body() data: any, @Req() request: RequestWithUser): Promise<any | EmptyObject> {

        const userRequest = request.user;

        const user = await this.authService.getUserByUsername(userRequest.username);

        if (!user || !user.email || !data || (!data.status && data.status != false) || !data.emailCode) throw ApiCauses.DATA_INVALID;

        try {
            const check = await this.authService.checkEmailCode(user, data.emailCode);

            if (!check) throw ApiCauses.USER_NOT_ACCESS;

            await this.authService.updateStatusEmailCode(user, data.status);

            return {};
        } catch (error) {
            console.log('sendEmailCode error: ', error);
            throw ApiCauses.INTERNAL_ERROR;
        }
    }

    @Post('/is-active-security')
    @ApiOperation({
        tags: ['auth'],
        operationId: 'is active 2fa',
        summary: 'is active 2fa',
        description: 'is active 2fa',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: LoginBase,
    })
    async isActiveSecurity(@Body() data: Login): Promise<LoginResponse | EmptyObject> {
        const user = await this.authService.validateUser(data);
        if (!user) {
            throw ApiCauses.EMAIL_OR_PASSWORD_INVALID;
        }

        if (!user.isActive2fa && user.isActiveEmailCode) this.authService.sendEmailCode(user);

        return {
            isActive2fa: user.isActive2fa,
            isActiveEmailCode: user.isActiveEmailCode
        };
    }

    @Post('/check-pass')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['auth'],
        operationId: 'check pass',
        summary: 'check pass',
        description: 'check pass',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: EmptyObjectBase,
    })
    async checkPass(@Req() request: RequestWithUser, @Body() data): Promise<EmptyObject> {
        const userRequest = request.user;

        const user = await this.authService.getUserByUsername(userRequest.username);

        if (!user) throw ApiCauses.DATA_INVALID;
        if (!data || !data.password) throw ApiCauses.DATA_INVALID;

        //verify hashed password and plain-password
        const isPassword = await argon2.verify(user.password, data.password);

        return {isPassword}
    }

    @Post('/get-2fa')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['auth'],
        operationId: 'get 2fa',
        summary: 'get 2fa',
        description: 'get 2fa',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: EmptyObjectBase,
    })
    async get2fa(@Req() request: RequestWithUser): Promise<EmptyObject> {
        const userRequest = request.user;

        const user = await this.authService.getUserByUsername(userRequest.username);

        if (!user) throw ApiCauses.DATA_INVALID;

        var secret = user.twoFactorAuthenticationSecret;
        if (!secret) {
            secret = await this.twoFactorAuthenticationService.generateTwoFactorAuthenticationSecret(user);
        }

        return {
            twoFactorAuthenticationSecret: this.jwtService.decode(secret)
        };
    }

    @Post('/active-2fa')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['auth'],
        operationId: 'active 2fa',
        summary: 'active 2fa',
        description: 'active 2fa',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: LoginBase,
    })
    async active2fa(@Body() data: any, @Req() request: RequestWithUser): Promise<any | EmptyObject> {

        const userRequest = request.user;

        const user = await this.authService.getUserByUsername(userRequest.username);

        if (!data.twofa) throw ApiCauses.TWOFA_INVALID;

        const isCodeValid = await this.twoFactorAuthenticationService.isTwoFactorAuthenticationCodeValid(
            data.twofa, user
        );

        if (!isCodeValid) throw ApiCauses.TWOFA_INVALID;

        if (!user.isActive2fa) {
            await this.usersService.turnOnTwoFactorAuthentication(user.id);
        }

        return {};
    }

    @Post('/disable-2fa')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['auth'],
        operationId: 'disable 2fa',
        summary: 'disable 2fa',
        description: 'disable 2fa',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: LoginBase,
    })
    async disable2fa(@Body() data: any, @Req() request: RequestWithUser): Promise<any | EmptyObject> {
        const userRequest = request.user;


        const user = await this.authService.getUserByUsername(userRequest.username);
        if (!data.twofa) throw ApiCauses.TWOFA_INVALID;

        const isCodeValid = await this.twoFactorAuthenticationService.isTwoFactorAuthenticationCodeValid(
            data.twofa, user
        );
        if (!isCodeValid) throw ApiCauses.TWOFA_INVALID;

        if (user.isActive2fa) {
            await this.usersService.turnOffTwoFactorAuthentication(user.id);
        }

        return {};
    }

    @Post('/login')
    @ApiOperation({
        tags: ['auth'],
        operationId: 'login',
        summary: 'Login',
        description: 'Login',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: LoginBase,
    })
    async login(@Body() data: Login): Promise<LoginResponse | EmptyObject> {
        const user = await this.authService.validateUser(data);
        if (!user) {
            throw ApiCauses.EMAIL_OR_PASSWORD_INVALID;
        }

        let isBlackList = await this.authService.validateUserEmailBlackList(data.email);
        if (!isBlackList) {
            throw ApiCauses.USER_IN_BLACKLIST;
        }

        if (user.isActive2fa) {

            if (!data.twofa) throw ApiCauses.TWOFA_INVALID;

            const isCodeValid = await this.twoFactorAuthenticationService.isTwoFactorAuthenticationCodeValid(
                data.twofa, user
            );

            if (!isCodeValid) throw ApiCauses.TWOFA_INVALID;
        }

        if (!user.isActive2fa && user.isActiveEmailCode) {

            if (!data.emailCode) throw ApiCauses.EMAIL_CODE_INVALID;

            const isEmailCodeValid = await this.authService.checkEmailCode(user, data.emailCode);

            if (!isEmailCodeValid) throw ApiCauses.EMAIL_CODE_INVALID;

        }


        return this.authService.login(user);
    }

    @Get('/info-user')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['auth'],
        operationId: 'info user',
        summary: 'info user',
        description: 'Info user',
    })
    async getInfoUser(@Req() request: RequestWithUser) {
        if (!request || !request.user) throw ApiCauses.DATA_INVALID;


        const user = await this.authService.getUserById(request.user.id);

        if (!user) throw ApiCauses.DATA_INVALID;

        const {password, token, twoFactorAuthenticationSecret, emailCode, ...dataReturn} = user;

        return dataReturn;
    }

    @Post('/logout')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['auth'],
        operationId: 'logout',
        summary: 'Logout',
        description: 'Logout',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: EmptyObjectBase,
    })
    async logout(@Req() request: any): Promise<EmptyObject> {
        const token = request.headers.authorization;
        this.authService.logout(token);
        return new EmptyObject();
    }

    @Post('/register-external-wallet')
    @ApiOperation({
        tags: ['auth'],
        operationId: 'register external request',
        summary: 'register external request',
        description: 'register external request',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async registerExternalWallet(@Body() data: RegisterWallet): Promise<RegisterResponse | EmptyObject> {

        // check address and signature
        const address = this._web3.eth.accounts.recover(process.env.SIGNATURE_TEXT, data.signature);


        if (!address || address != data.address) throw ApiCauses.INVALID_SIGNATURE_WALLET;

        let user = await this.authService.getUserByData(data);

        if (user) throw ApiCauses.DUPLICATED_EMAIL_OR_USERNAME;

        user = await this.authService.createUserByWallet(data);

        return user;
    }

    @Post('/login-external-wallet')
    @ApiOperation({
        tags: ['auth'],
        operationId: 'login external request',
        summary: 'login external request',
        description: 'login external request',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async loginExternalWallet(@Body() data: LoginWallet): Promise<RegisterResponse | EmptyObject> {

        // check address and signature
        const address = this._web3.eth.accounts.recover(process.env.SIGNATURE_TEXT, data.signature);


        if (!address || address != data.address) throw ApiCauses.INVALID_SIGNATURE_WALLET;

        const user = await this.authService.getUserByData(data);
        //if (!user) throw ApiCauses.USER_ERROR;
        if (!user || user && user.status == 'request') throw ApiCauses.USER_ERROR;

        return this.authService.login(user);
    }

    @Get('countries')
    @ApiOperation({
        tags: ['auth'],
        operationId: 'list countries',
        summary: 'Get all countries',
        description: 'Get all countries',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: Countries,
    })
    @ApiQuery({
        name: 'page',
        required: false,
        type: Number,
    })
    @ApiQuery({
        name: 'limit',
        required: false,
        type: Number,
    })
    async getCountriesList(
        @Query('page', new DefaultValuePipe(1)) page: number,
        @Query('limit', new DefaultValuePipe(10)) limit: number,
    ): Promise<PaginationResponse<Countries>> {
        return this.authService.getCountriesList({page, limit});
    }


    @Post('/connect-verdant-wallet')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['auth'],
        operationId: 'register external request',
        summary: 'register external request',
        description: 'register external request',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async connectVerdantWallet(@Req() request: any): Promise<RegisterResponse | EmptyObject> {

        if (!request || !request.user || !request.user.id || !request.user.username) throw ApiCauses.USER_NOT_ACCESS;

        const userData = request.user;

        const user = await this.authService.connectVerdantWallet(userData);

        return user;
    }

    @Post('/connect-external-wallet')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['auth'],
        operationId: 'register external request',
        summary: 'register external request',
        description: 'register external request',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async connectExternalWallet(@Body() data: ExternalWallet, @Req() request: any): Promise<RegisterResponse | EmptyObject> {

        if (!request || !request.user || !request.user.id || !request.user.username) throw ApiCauses.USER_NOT_ACCESS;


        // check address and signature
        const address = this._web3.eth.accounts.recover(process.env.SIGNATURE_TEXT, data.signature);


        if (!address || address != data.address) throw ApiCauses.INVALID_SIGNATURE_WALLET;

        const checkWallet = await this.authService.getUserByData(data);
        if (checkWallet && checkWallet.wallet) throw ApiCauses.DUPLICATED_EMAIL_OR_USERNAME;

        const userData = request.user;

        const user = await this.authService.connectExternalWallet(data, userData);

        return user;
    }


    @Get('address-book')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['auth'],
        operationId: 'list address book',
        summary: 'Get all address book',
        description: 'Get all address book',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: User,
    })
    @ApiQuery({
        name: 'page',
        required: false,
        type: Number,
    })
    @ApiQuery({
        name: 'limit',
        required: false,
        type: Number,
    })
    async getListAddressBook(
        @Query('page', new DefaultValuePipe(1)) page: number,
        @Query('limit', new DefaultValuePipe(10)) limit: number,
    ): Promise<PaginationResponse<User>> {
        return this.authService.getListAddressBook({page, limit});
    }


}
