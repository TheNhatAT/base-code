import {Body, Controller, DefaultValuePipe, Get, HttpStatus, Param, Post, Query, Req, UseGuards} from '@nestjs/common';
import {AuthService} from './auth.service';
import {ApiOperation, ApiQuery, ApiResponse} from '@nestjs/swagger';
import {ApiCauses} from '../../config/exception/apiCauses';
import {JwtAuthGuard} from '../admin/jwt-auth.guard';
import {RegisterBase} from './response/registerBase.dto';
import {TwoFactorAuthenticationService} from './twoFactorAuthentication.service';
import {UsersService} from './user.service';
import {JwtService} from '@nestjs/jwt';
import {PaginationResponse} from 'src/config/rest/paginationResponse';
import {User} from '../../database/entities';
import {AuthService as AuthServiceAdmin} from '../admin/auth.service';

const Web3 = require("web3");

@Controller('user-admin')
export class UserAdminController {
    _web3 = new Web3();

    constructor(
        private jwtService: JwtService,
        private readonly twoFactorAuthenticationService: TwoFactorAuthenticationService,
        private readonly usersService: UsersService,
        private authService: AuthService,
        private authServiceAdmin: AuthServiceAdmin,
    ) {
    }

    @Post('/update-user')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['user-admin'],
        operationId: 'update user',
        summary: 'update user',
        description: 'update user',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async updateProfile(@Body() data: any, @Req() request: any) {
        if (!request || !request.user) throw ApiCauses.USER_NOT_ACCESS;
        const checkPermission = await this.authServiceAdmin.checkPermissionUser(request.user);
        if (!checkPermission) {
            throw ApiCauses.USER_NOT_ACCESS;
        }

        const userUpdate = await this.authService.updateBrandToUser(data);

        if (!userUpdate) throw ApiCauses.DATA_INVALID;

        return userUpdate;
    }

    @Post('/update-user-v2')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['user-admin'],
        operationId: 'update user',
        summary: 'update user',
        description: 'update user',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async updateProfileV2(@Body() data: any, @Req() request: any) {
        if (!request || !request.user) throw ApiCauses.USER_NOT_ACCESS;
        const checkPermission = await this.authServiceAdmin.checkPermissionUser(request.user);
        if (!checkPermission) {
            throw ApiCauses.USER_NOT_ACCESS;
        }

        const userUpdate = await this.authService.updateBrandToUserV2(data);

        if (!userUpdate) throw ApiCauses.DATA_INVALID;

        return userUpdate;
    }

    @Get('list')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['user-admin'],
        operationId: 'getList admin',
        summary: 'Get all admins',
        description: 'Get all admins',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: User,
    })
    @ApiQuery({
        name: 'username',
        required: false,
        type: String,
    })
    @ApiQuery({
        name: 'email',
        required: false,
        type: String,
    })
    @ApiQuery({
        name: 'wallet',
        required: false,
        type: String,
    })
    @ApiQuery({
        name: 'status',
        required: false,
        type: String,
    })
    @ApiQuery({
        name: 'page',
        required: false,
        type: Number,
    })
    @ApiQuery({
        name: 'limit',
        required: false,
        type: Number,
    })
    async getList(
        @Query('page', new DefaultValuePipe(1)) page: number,
        @Query('limit', new DefaultValuePipe(10)) limit: number,
        @Query('username') username: string,
        @Query('email') email: string,
        @Query('wallet') wallet: string,
        @Query('status') status: string,
        @Req() request: any
    ): Promise<PaginationResponse<User>> {
        if (!request || !request.user) throw ApiCauses.USER_NOT_ACCESS;
        const checkPermission = await this.authServiceAdmin.checkPermissionUser(request.user);
        if (!checkPermission) {
            throw ApiCauses.USER_NOT_ACCESS;
        }

        return this.authService.getList({username, email, wallet, status}, {page, limit});
    }

    @Get('list-v2')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['user-admin'],
        operationId: 'getList admin',
        summary: 'Get all admins',
        description: 'Get all admins',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: User,
    })
    @ApiQuery({
        name: 'username',
        required: false,
        type: String,
    })
    @ApiQuery({
        name: 'email',
        required: false,
        type: String,
    })
    @ApiQuery({
        name: 'wallet',
        required: false,
        type: String,
    })
    @ApiQuery({
        name: 'status',
        required: false,
        type: String,
    })
    @ApiQuery({
        name: 'page',
        required: false,
        type: Number,
    })
    @ApiQuery({
        name: 'limit',
        required: false,
        type: Number,
    })
    async getListV2(
        @Query('page', new DefaultValuePipe(1)) page: number,
        @Query('limit', new DefaultValuePipe(10)) limit: number,
        @Query('username') username: string,
        @Query('email') email: string,
        @Query('wallet') wallet: string,
        @Query('status') status: string,
        @Req() request: any
    ): Promise<PaginationResponse<User>> {
        if (!request || !request.user) throw ApiCauses.USER_NOT_ACCESS;
        const checkPermission = await this.authServiceAdmin.checkPermissionUser(request.user);
        if (!checkPermission) {
            throw ApiCauses.USER_NOT_ACCESS;
        }
        return this.authService.getListV2({username, email, wallet, status}, {page, limit});
    }

    @Get('user/:id')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['user-admin'],
        operationId: 'info user',
        summary: 'info user',
        description: 'Info user',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: User,
    })
    async getInfoUser(@Param('id') id: number, @Req() request: any) {
        if (!request || !request.user) throw ApiCauses.USER_NOT_ACCESS;
        const checkPermission = await this.authServiceAdmin.checkPermissionUser(request.user);
        if (!checkPermission) {
            throw ApiCauses.USER_NOT_ACCESS;
        }
        const user = await this.authService.getDetailUser(id);
        if (!user) throw ApiCauses.DATA_INVALID;
        return user;
    }


    @Get('detail-user/:id')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['user-admin'],
        operationId: 'info user',
        summary: 'info user',
        description: 'Info user',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: User,
    })
    async getDetailUser(@Param('id') id: number, @Req() request: any) {
        if (!request || !request.user) throw ApiCauses.USER_NOT_ACCESS;
        const checkPermission = await this.authServiceAdmin.checkPermissionUser(request.user);
        if (!checkPermission) {
            throw ApiCauses.USER_NOT_ACCESS;
        }
        const user = await this.authService.getDetailUser(id);
        if (!user) throw ApiCauses.DATA_INVALID;
        return user;
    }

    @Get('detail-user-v2/:id')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['user-admin'],
        operationId: 'info user',
        summary: 'info user',
        description: 'Info user',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: User,
    })
    async getDetailUserV2(@Param('id') id: number, @Req() request: any) {
        if (!request || !request.user) throw ApiCauses.USER_NOT_ACCESS;
        const checkPermission = await this.authServiceAdmin.checkPermissionUser(request.user);
        if (!checkPermission) {
            throw ApiCauses.USER_NOT_ACCESS;
        }
        const user = await this.authService.getDetailUserV2(id);
        if (!user) throw ApiCauses.DATA_INVALID;
        return user;
    }

    @Get('black-list')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['user-admin'],
        operationId: 'getList black-list',
        summary: 'Get all black-list',
        description: 'Get all black-list',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: User,
    })
    @ApiQuery({
        name: 'name',
        required: false,
        type: String,
    })
    @ApiQuery({
        name: 'page',
        required: false,
        type: Number,
    })
    @ApiQuery({
        name: 'limit',
        required: false,
        type: Number,
    })
    async getBlackList(
        @Query('page', new DefaultValuePipe(1)) page: number,
        @Query('limit', new DefaultValuePipe(10)) limit: number,
        @Query('name') name: string,
        @Req() request: any
    ): Promise<PaginationResponse<User>> {
        if (!request || !request.user) throw ApiCauses.USER_NOT_ACCESS;
        const checkPermission = await this.authServiceAdmin.checkPermissionUser(request.user);
        if (!checkPermission) {
            throw ApiCauses.USER_NOT_ACCESS;
        }
        return this.authService.getBlackList({name}, {page, limit});
    }

    @Post('/add-black-list')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['user-admin'],
        operationId: 'update add-black-list',
        summary: 'update add-black-list',
        description: 'update add-black-list',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async addBlackList(@Body() data: any, @Req() request: any) {
        if (!request || !request.user) throw ApiCauses.USER_NOT_ACCESS;
        const checkPermission = await this.authServiceAdmin.checkPermissionUser(request.user);
        if (!checkPermission) {
            throw ApiCauses.USER_NOT_ACCESS;
        }
        if (!data || !data.wallet) throw ApiCauses.DATA_INVALID;
        const userUpdate = await this.authService.addBlackList(data.wallet);
        return userUpdate;
    }

    @Post('/add-white-list')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['user-admin'],
        operationId: 'update add-white-list',
        summary: 'update add-white-list',
        description: 'update add-white-list',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async addWhiteList(@Body() data: any, @Req() request: any) {
        if (!request || !request.user) throw ApiCauses.USER_NOT_ACCESS;
        const checkPermission = await this.authServiceAdmin.checkPermissionUser(request.user);
        if (!checkPermission) {
            throw ApiCauses.USER_NOT_ACCESS;
        }
        if (!data || !data.wallet) throw ApiCauses.DATA_INVALID;
        const userUpdate = await this.authService.addWhiteList(data.wallet);
        return userUpdate;
    }

    @Post('/remove-white-black-list')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['user-admin'],
        operationId: 'update remove-white-black-list',
        summary: 'update remove-white-black-list',
        description: 'update remove-white-black-list',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: RegisterBase,
    })
    async removeBlackWhiteList(@Body() data: any, @Req() request: any) {
        if (!request || !request.user) throw ApiCauses.USER_NOT_ACCESS;
        const checkPermission = await this.authServiceAdmin.checkPermissionUser(request.user);
        if (!checkPermission) {
            throw ApiCauses.USER_NOT_ACCESS;
        }
        if (!data || !data.wallet) throw ApiCauses.DATA_INVALID;
        const userUpdate = await this.authService.removeBlackWhiteList(data.wallet);
        return userUpdate;
    }

    @Get('white-list')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['user-admin'],
        operationId: 'white-list user',
        summary: 'Get all white-list',
        description: 'Get all white-list',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: User,
    })
    @ApiQuery({
        name: 'address',
        required: false,
        type: String,
    })
    @ApiQuery({
        name: 'name',
        required: false,
        type: String,
    })
    @ApiQuery({
        name: 'page',
        required: false,
        type: Number,
    })
    @ApiQuery({
        name: 'limit',
        required: false,
        type: Number,
    })
    async getWhiteList(
        @Query('page', new DefaultValuePipe(1)) page: number,
        @Query('limit', new DefaultValuePipe(10)) limit: number,
        @Query('name') name: string,
        @Query('address') address: string,
        @Req() request: any
    ): Promise<PaginationResponse<User>> {
        if (!request || !request.user) throw ApiCauses.USER_NOT_ACCESS;
        const checkPermission = await this.authServiceAdmin.checkPermissionUser(request.user);
        if (!checkPermission) {
            throw ApiCauses.USER_NOT_ACCESS;
        }
        return this.authService.getWhiteList({name, address}, {page, limit});
    }

    @Get('address-book')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['user-admin'],
        operationId: 'list address book',
        summary: 'Get all address book',
        description: 'Get all address book',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: User,
    })
    @ApiQuery({
        name: 'page',
        required: false,
        type: Number,
    })
    @ApiQuery({
        name: 'limit',
        required: false,
        type: Number,
    })
    async getListAddressBook(
        @Query('page', new DefaultValuePipe(1)) page: number,
        @Query('limit', new DefaultValuePipe(10)) limit: number,
    ): Promise<PaginationResponse<User>> {
        return this.authService.getListAddressBook({page, limit});
    }


    @Get('list-vender')
    @UseGuards(JwtAuthGuard)
    @ApiOperation({
        tags: ['user-admin'],
        operationId: 'list list-vender',
        summary: 'Get list-vender',
        description: 'Get all list-vender',
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'Successful',
        type: User,
    })
    @ApiQuery({
        name: 'page',
        required: false,
        type: Number,
    })
    @ApiQuery({
        name: 'limit',
        required: false,
        type: Number,
    })
    async getListVender(
        @Query('page', new DefaultValuePipe(1)) page: number,
        @Query('limit', new DefaultValuePipe(10)) limit: number,
    ): Promise<PaginationResponse<User>> {
        return this.authService.getListVender({page, limit});
    }
}
