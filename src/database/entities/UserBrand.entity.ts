import {BeforeInsert, BeforeUpdate, Column, Entity, PrimaryGeneratedColumn} from 'typeorm';
import {nowInMillis} from '../../shared/Utils';

@Entity('user_brand')
export class UserBrand {
    @PrimaryGeneratedColumn({name: 'id', type: 'int'})
    public id: number;

    @Column({name: 'user_id', type: 'int', nullable: false})
    public userId: number;

    @Column({name: 'brand_id', type: 'int', nullable: false})
    public brandId: number;

    @Column({name: 'created_at', type: 'bigint', nullable: true})
    public createdAt: number;

    @Column({name: 'updated_at', type: 'bigint', nullable: true})
    public updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }

}
