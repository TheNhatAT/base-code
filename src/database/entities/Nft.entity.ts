import {BeforeInsert, BeforeUpdate, Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm';
import {nowInMillis} from '../../shared/Utils';

@Entity('nft')
@Index('slug', ['slug'], {unique: false})
@Index('status', ['status'], {unique: false})
@Index('type', ['type'], {unique: false})
@Index('owner_id', ['ownerId'], {unique: false})
@Index('collection_id', ['collectionId'], {unique: false})

export class Nft {
    @PrimaryGeneratedColumn({name: 'id', type: 'int'})
    public id: number;

    @Column({name: 'collection_id', type: 'int', nullable: false})
    public collectionId: number;

    @Column({name: 'collection_name', type: 'varchar', nullable: false})
    public collectionName: string;

    @Column({name: 'name', type: 'varchar', length: 255, nullable: false})
    public name: string;

    @Column({name: 'slug', type: 'varchar', length: 255, nullable: false})
    public slug: string;

    @Column({name: 'image_url', type: 'text', nullable: false})
    public imageUrl: string;

    @Column({name: 'description', type: 'text', nullable: true})
    public description: string;

    @Column({name: 'quantity', type: 'int', unsigned: true, nullable: false})
    public quantity: number;

    @Column({name: 'payment_token', type: 'varchar', length: 20, nullable: true})
    public paymentToken: string;

    @Column({name: 'price', type: 'varchar', nullable: false})
    public price: string;

    @Column({name: 'status', type: 'varchar', length: 25, nullable: true, default: "minting"})
    public status: string;

    @Column({name: 'type', type: 'varchar', length: 25, nullable: true})
    public type: string;

    @Column({name: 'data', type: 'text', nullable: true})
    public data: string;

    @Column({name: 'owner_id', type: 'int', nullable: false})
    public ownerId: number;

    @Column({name: 'owner_name', type: 'varchar', length: 80, nullable: false})
    public ownerName: string;

    @Column({name: 'created_at', type: 'bigint', nullable: false})
    public createdAt: number;

    @Column({name: 'updated_at', type: 'bigint', nullable: false})
    public updatedAt: number;

    @BeforeInsert()
    public updateCreatedAt() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
