import {BeforeInsert, BeforeUpdate, Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm';
import {nowInMillis} from '../../shared/Utils';

@Entity('collection')
@Index('slug', ['slug'], {unique: false})
@Index('status', ['status'], {unique: false})
@Index('type', ['type'], {unique: false})
@Index('owner_id', ['ownerId'], {unique: false})
@Index('address', ['address'], { unique: false })
export class Collection {
    @PrimaryGeneratedColumn({name: 'id', type: 'int'})
    public id: number;

    @Column({name: 'name', type: 'varchar', length: 255, nullable: false})
    public name: string;

    @Column({name: 'slug', type: 'varchar', length: 255, nullable: false})
    public slug: string;

    @Column({name: 'image_url', type: 'text', nullable: false})
    public imageUrl: string;

    @Column({name: 'description', type: 'text', nullable: true})
    public description: string;

    @Column({name: 'payment_token', type: 'varchar', length: 20, nullable: false})
    public paymentToken: string;

    @Column({name: 'status', type: 'varchar', length: 25, nullable: true})
    public status: string;

    @Column({ name: 'chain_id', type: 'varchar', nullable: true })
    public chainId: string;

    @Column({ name: 'address', type: 'varchar', length: 150, unique: true})
    public address: string;

    @Column({name: 'type', type: 'varchar', length: 25, nullable: true})
    public type: string;

    @Column({name: 'is_show', type: 'tinyint', width: 1, nullable: true, default: 0})
    public isShow: boolean;

    @Column({name: 'index_show', type: 'int', nullable: true, default: null})
    public indexShow: number;

    @Column({name: 'data', type: 'text', nullable: true})
    public data: string;

    @Column({name: 'owner_id', type: 'int', nullable: true})
    public ownerId: number;

    @Column({name: 'owner_name', type: 'varchar', length: 80, nullable: true})
    public ownerName: string;

    @Column({name: 'brand_id', type: 'int', nullable: true, default: null})
    public brandId: number;

    @Column({name: 'category_id', type: 'int', nullable: true, default: null})
    public categoryId: number;

    @Column({ name: 'sync_status', type: 'varchar', length: 25, nullable: true})
    public syncStatus: string;

    @Column({name: 'created_at', type: 'bigint', nullable: false})
    public createdAt: number;

    @Column({name: 'updated_at', type: 'bigint', nullable: false})
    public updatedAt: number;


    @BeforeInsert()
    public updateCreatedAt() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
