import {BeforeInsert, BeforeUpdate, Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm';
import {nowInMillis} from '../../shared/Utils';

@Entity('nft_owner')
@Index('chain_id', ['chainId'], { unique: false })
@Index('contract_address', ['contractAddress'], { unique: false })
@Index('', ['contractAddress', 'tokenId', 'owner'], { unique: true })  
@Index('owner', ['owner'], { unique: false })
@Index('amount', ['amount'], { unique: false })
@Index('block_timestamp', ['blockTimestamp'], { unique: false })
export class NftOwner {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;

    @Column({ name: 'chain_id', type: 'varchar', nullable: true })
    public chainId: string;

    @Column({ name: 'contract_address', type: 'varchar', nullable: false })
    public contractAddress: string;

    @Column({ name: 'token_id', type: 'varchar', nullable: false })
    public tokenId: string;

    @Column({ name: 'owner', type: 'varchar', length: 50, nullable: true })
    owner: string;

    @Column({ name: 'amount', type: 'int', nullable: false })
    amount: number;

    @Column({ name: 'block_timestamp', type: 'bigint', nullable: true })
    public blockTimestamp: number;
  
    @Column({ name: 'status', type: 'varchar', length: 50, nullable: true })
    status: string;
    
    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    public createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    public updatedAt: number;

    @BeforeInsert()
    public updateCreatedAt() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
