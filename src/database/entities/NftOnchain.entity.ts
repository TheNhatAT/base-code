import {BeforeInsert, BeforeUpdate, Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm';
import {nowInMillis} from '../../shared/Utils';

@Entity('nft_onchain')
@Index('chain_id', ['chainId'], { unique: false })
@Index('contract_address', ['contractAddress'], { unique: false })
@Index('t', ['chainId', 'contractAddress', 'tokenId'], { unique: true })  
@Index('token_uri', ['tokenUri'], { unique: false })
@Index('image', ['image'], { unique: false })
@Index('block_timestamp', ['blockTimestamp'], { unique: false })
@Index('metadata_updated_at', ['metadataUpdatedAt'], { unique: false })
@Index('status', ['status'], { unique: false })
@Index('display_status', ['displayStatus'], { unique: false })
@Index('collection_key', ['chainId', 'contractAddress'], { unique: false })
export class NftOnchain {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;

    @Column({ name: 'chain_id', type: 'varchar', nullable: true })
    public chainId: string;

    @Column({ name: 'contract_address', type: 'varchar', nullable: false })
    public contractAddress: string;

    @Column({ name: 'token_id', type: 'varchar', nullable: false })
    public tokenId: string;

    @Column({ name: 'creator', type: 'varchar', length: 50, nullable: true })
    public creator: string;

    @Column({ name: 'token_uri', type: 'varchar', length: 255, nullable: false })
    public tokenUri: string;

    @Column({ name: 'name', type: 'varchar', length: 255, nullable: true })
    public name: string;

    @Column({ name: 'image', type: 'varchar', length: 255, nullable: true })
    public image: string;

    @Column({ name: 'origin_image', type: 'varchar', length: 255, nullable: true })
    public originImage: string;

    @Column({ name: 'content_type', type: 'varchar', length: 50, nullable: true })
    public contentType: string;
  
    @Column({ name: 'metadata', type: 'varchar', length: 5000, nullable: true })
    public metadata: string;

    @Column({ name: 'block_timestamp', type: 'bigint', nullable: true })
    public blockTimestamp: number;
  
    @Column({ name: 'status', type: 'varchar', length: 50, nullable: true })
    public status: string;

    @Column({ name: 'display_status', type: 'varchar', length: 50, nullable: true })
    public displayStatus: string;
    
    @Column({ name: 'metadata_updated_at', type: 'bigint', nullable: false })
    public metadataUpdatedAt: number;
    
    @Column({ name: 'ipfs_updated_at', type: 'bigint', nullable: false })
    public ipfsUpdatedAt: number;

    @Column({ name: 'is_ipfs_updated', type: 'bool', nullable: false, default: false })
    public isIpfsUpdated: boolean;

    @Column({ name: 'is_metadata_updated', type: 'bool', nullable: false, default: false })
    public isMetadataUpdated: boolean;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    public createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    public updatedAt: number;

    @BeforeInsert()
    public updateCreatedAt() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
