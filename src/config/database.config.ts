import {DataSourceOptions} from 'typeorm';
import {
    Address,
    Admin,
    Config,
    Countries,
    CurrencyConfig,
    CurrencyToken,
    KmsCmk,
    KmsDataKey,
    MetaData,
    Nft,
    NftLog,
    NftOnchain,
    User,
    UserBrand
} from '../database/entities';
import {Collection} from "../database/entities/Collection.entity";

require('dotenv').config()


export const databaseConfig: DataSourceOptions = {
    type: (process.env.TYPEORM_CONNECTION || 'mysql') as any,
    host: process.env.TYPEORM_HOST || 'localhost',
    port: parseInt(process.env.TYPEORM_PORT) || 3306,
    username: process.env.TYPEORM_USERNAME || 'username',
    password: process.env.TYPEORM_PASSWORD || 'password',
    database: process.env.TYPEORM_DATABASE || 'database',
    entities: [
        Admin,
        Address,
        CurrencyConfig,
        KmsCmk,
        KmsDataKey,
        Collection,
        Config,
        Countries,
        CurrencyToken,
        MetaData,
        Nft,
        NftOnchain,
        NftLog,
        User,
        UserBrand
    ],
    synchronize: true,
};
